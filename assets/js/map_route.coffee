Date.try = (date_value)->
  if date_value then new Date(date_value) else ""


currentTimestamp = 0
map = null
busstops = {}
route = {}

initMap = ->
  map = L.map 'map-route', {
    zoomControl: false
  }
  map.setView([58.600060, 49.665317], 13)
  map.locate({
    setView: false
  })
  L.control.zoom(
    position: 'topright'
  ).addTo(map)
  L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 20
  }).addTo(map)
  # button = new L.Control.Button({
  #   position: 'topright',
  # }).addTo(map)




ready = ->
  if map_route_tag = document.querySelector("#map-route")
    route.id = map_route_tag.dataset.routeId
    route.marsh = map_route_tag.dataset.routeMarsh
    initMap()
    # getBusstops()
    getRoute()
    # setInterval(getRoute, 1000)


busstopIcon = new (L.icon)({iconUrl: "https://m.cdsvyatka.com/stop20.png"})
getBusstops = ->
  fetch "/api/json/busstops"#, {credentials: 'include'}
  .then (resp)-> resp.json()
  .then (data)->
    for busstop in data
      busstopMarker = new (L.Marker)([busstop.lat, busstop.lon || busstop.lng], {
        icon: busstopIcon
        iconAnchor: [12, 12]
      }).addTo(map)
      busstopMarker.bindPopup("<span>#{busstop.stop_name}</span>")
      busstop.marker = busstopMarker
      busstops[busstop.kod] = busstop
# getBusstops()

changeLocation = (machine)->
  if machine and machine.locations
    machine.current_location++
    machine.current_location = 0 if machine.locations.length <= machine.current_location

    loc = machine.locations[machine.current_location]
    marker = machine.marker
    if marker
      console.log('relocate marker')
      # new_marker = (L.Marker.movingMarker)(
      #   [
      #     [old_loc.lat, old_loc.lon || old_loc.lng],
      #     [loc.lat, loc.lon || loc.lng],
      #     # icon: getCdsMarker(loc.marsh, loc.marshnum, loc.angle)
      #   ], [1000]
      # ).addTo(map)
      # map.removeLayer(marker)
      # marker = new_marker
      marker.setLatLng([loc.lat, loc.lon || loc.lng]);
    else
      console.log('new marker')
      marker = new (L.Marker)([loc.lat, loc.lon || loc.lng], {
        # icon: getCdsMarker(loc.marsh, loc.marshnum, loc.angle)
        title: loc.marshnum
      }).addTo(map)

    marker.bindPopup("<span>#{loc.marshnum} (#{loc.marsh})</span><br><span>#{machine.name}</span><br><span>#{loc.info}</span>")
    machine.marker = marker

    pause = (loc.pause || 0) / 1000 * 5
    # console.log(pause)
    # if loc.timestamp - currentTimestamp > (1000*60)
    #   document.title = (currentTimestamp = loc.timestamp).toLocaleTimeString()
    setTimeout(changeLocation, pause, machine)


busIcon = new (L.icon)({iconUrl: "https://m.cdsvyatka.com/stop20.png"})
getRoute = ->
  fetch "/api/json/routes/#{route.id}/machines?route=#{route.marsh}"#, {credentials: 'include'}
  .then (resp)-> resp.json()
  .then (data)->
    for machine in data
      machine.name = data.machine_name

      interval = 1000 * 60 * 60 * 24
      old_timestamp = Math.floor(Date.now() / interval) * interval
      old_timestamp += (-1000 * 60 * 60 * 3) + 1000 * 60 * 60 * 11
      for location in machine.locations
        location.timestamp = Date.try(location.timestamp)
        if old_timestamp
          location.pause = location.timestamp.getTime() - old_timestamp
        old_timestamp = location.timestamp.getTime()
      machine.current_location = -1
      loc = machine.locations[0]
      pause = (loc.pause || 0) / 1000 * 5
      # console.log(pause)
      setTimeout(changeLocation, pause, machine)







document.addEventListener "DOMContentLoaded", ready
