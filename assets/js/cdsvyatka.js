/*
    комментари всеврьез не воспринимать.
    просто надо рахобраться с кодом.
    возможно все не так плохо и все ходы вынужденные и необходимые.
    хотя попозже посмотрим */



if (marshnum) { // тайтл
  document.title = "Маршрут " + marshnum;
}

// "Вы здесь"
var mytargeticon = L.icon({
  iconUrl: 'target1.png',
  iconAnchor: [12, 12]
});

var locations = {}; // бусы
var marker_arr = {}; // nothing

/* настройка лифлета */
var map = L.map('map', {
  zoomControl: false
});
if (city == 2) // Чепяга
  map.setView([58.556743, 50.044124], 13);
else // Киров
  map.setView([58.600060, 49.665317], 13);
L.control.zoom({
  position: 'bottomright'
}).addTo(map);
var button = new L.Control.Button({
  position: 'bottomright',
  on: enable_busstop,
  onFunc: onFunc,
  offFunc: offFunc
}).addTo(map);
var geobutton = new L.Control.Geolocation({
  position: 'bottomright',
  Func: locateFunc
}).addTo(map);
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
  maxZoom: 18,
  //шифровка намбер 0
  attribution: 'Map data � openstreetmap contributors'
}).addTo(map);
//map.locate({setView: true, maxZoom: 13});
map.locate({
  setView: false
});
/* настройка лифлета */


/* "Вы здесь" */
function onLocationFound(e) {

  var radius = e.accuracy / 2;
  if (targetMarker != null && targetMarker != undefined) {
    if (map.hasLayer(targetMarker))
      map.removeLayer(targetMarker);
  }
  targetMarker = L.marker(e.latlng, {
      icon: mytargeticon
    }).addTo(map)
    .bindPopup("Вы здесь");
  if (!boundsfit)
    map.setView(e.latlng, 13)
}
map.on('locationfound', onLocationFound);
/* "Вы здесь" */

function onLocationError(e) {
  // alert(e.message);
}

// возврат карты в дефолт
function locateFunc() {
  //boundsfit=false;
  map.locate({
    setView: true,
    maxZoom: 13
  });
}

// остановки вкл
function onFunc() {
  if (data != null && data != undefined && data["busstop"]) {
    var myicon = L.icon({
      iconUrl: 'stop20.png',
      iconAnchor: [12, 12]

    });

    var stop_arr = data["busstop"];
    for (key in stop_arr) {
      if (stop_arr[key].marker == null || stop_arr[key].marker == undefined) {
        var busmarker = new L.Marker([stop_arr[key].lat, stop_arr[key].lng], {
          icon: myicon
        });;
        // а вот тут бы яаксом подгрузить но ЗАЧЕМ? или хотябы _blank
        busmarker.bindPopup("<a href=prediction.php?busstop=" + stop_arr[key].kod + ">" + stop_arr[key].stop_name + "</a>");
        stop_arr[key].marker = busmarker;
      }
      if (!map.hasLayer(stop_arr[key].marker))
        stop_arr[key].marker.addTo(map);
    }
  }
}
// остановки выкл
function offFunc() {
  if (data != null && data != undefined && data["busstop"]) {
    var myicon = L.icon({
      iconUrl: 'stop20.png',
      iconAnchor: [12, 12]

    });

    var stop_arr = data["busstop"];
    for (key in stop_arr) {
      if (stop_arr[key].marker) {
        if (map.hasLayer(stop_arr[key].marker))
          map.removeLayer(stop_arr[key].marker);
        stop_arr[key].marker = null;
      }
    }
  }
}

// редро(рерайт) маркеров (хотя не маркеров а локаций ибо (marker_arr={}))
// но черт с ним. итак сойдет
function updateMarkersOnMap() {
  if (locations !== undefined && locations !== null) {
    var bounds = map.getBounds();
    var southwest = bounds.getSouthWest();
    var northeast = bounds.getNorthEast();

    for (var key in locations) {
      //шифровка намбер 1
      //��������, ������ �� ������ � ������ �������
      var loc = locations[key];
      if (loc.lat <= northeast.lat && loc.lat >= southwest.lat && loc.lng <= northeast.lng && loc.lng >= southwest.lng) {
        if (loc.marker == undefined || loc.marker == null) {
          //создаем маркер, ставим на карту,вешаем попап
          locations[key].marker = new L.Marker([loc.lat, loc.lng], {
            icon: getCdsMarker(loc.marsh, loc.marshnum, loc.angle),
            opacity: 1
          });
          //locations[key].marker=new L.Marker([loc.lat,loc.lng], {icon: circle, opacity: 1});
          locations[key].marker.addTo(map);
          locations[key].marker.bindPopup(locations[key].info);
        } else {
          //маркер ужо есть: ставим на карту (если не был поставлен), перемещаем. DRY is bullshit
          if (locations[key].marker.map == null || locations[key].marker.map == undefined)
            locations[key].marker.addTo(map);
          // да и почемуто не ворочаем его, а тупо релокейт. непонятно
          locations[key].marker.setLatLng([loc.lat, loc.lng]);
        }
      } else {
        //шифровка намбер 2
        //������ �� ������ � ����� - ������ ���
        if (locations[key].marker !== null && locations[key].marker !== undefined) {
          // экономим ресурсы??
          if (map.hasLayer(locations[key].marker))
            map.removeLayer(locations[key].marker);
          locations[key].marker = null;
        }

      }
    }
  }

}
map.on('locationerror', onLocationError);
map.on('viewreset', updateMarkersOnMap);
map.on('moveend', updateMarkersOnMap);
map.on('dragend', updateMarkersOnMap);
map.on('zoomend', updateMarkersOnMap);
map.on('resize', updateMarkersOnMap);

// рисуем бус, ставим номер, поворачиваем
function getCdsMarker(marsh, marshnum, angle) {
  var fillcolor = "#F00";
  if (marsh > 5000) fillcolor = "blue"; // межгород? или трамвай?
  var cdsmarker = new L.Icon.Canvas({
    iconSize: new L.Point(60, 60),
    iconAnchor: new L.Point(30, 30)
  });
  cdsmarker.draw = function(ctx, w, h) {
    ctx.translate(30, 30);
    ctx.rotate(angle * Math.PI / 180);
    ctx.beginPath();
    ctx.lineWidth = 2;
    ctx.strokeStyle = '#FFF';
    ctx.fillStyle = fillcolor;
    ctx.arc(0, 0, 14, -60 * Math.PI / 180, 240 * Math.PI / 180, false);
    ctx.fill();
    ctx.stroke();
    ctx.closePath();
    ctx.beginPath();
    ctx.moveTo(8, -12);
    ctx.lineTo(0, -25);
    ctx.lineTo(-8, -12);
    ctx.fill();
    ctx.stroke();
    ctx.closePath();
    ctx.rotate(-angle * Math.PI / 180);
    ctx.fillStyle = '#FFF';
    ctx.font = 'bold 16px ';
    ctx.fillText(marshnum, -8, 4);
  }
  return cdsmarker;
}

// получаем маршрут и остановки
if (enable_scheme || enable_busstop)
  var jqxhr = $.ajax({
      url: "scheme.php?marsh=" + marsh,
      dataType: "json"
    })
    .done(function(data) {
      drawMarshScheme(data);
      // alert( "success" );
    })
    .fail(function() {
      // alert( "error" );
    })
    .always(function() {
      // alert( "complete" );
    });


// рисуем маршрут и остановки
// неинтересно
function drawMarshScheme(data1) {
  if (data1) {
    data = data1;
    if (data["scheme"] && enable_scheme) {
      var linearr = [];
      var scheme_arr = data["scheme"];
      for (key in scheme_arr) {
        linearr[key] = L.latLng(scheme_arr[key].lat, scheme_arr[key].lng);
      }

      var polyline = L.polyline(linearr, {
        color: 'green'
      });
      polyline.addTo(map);

      var bounds = polyline.getBounds();
      if (bounds != undefined && bounds != null) {
        boundsfit = true;
        map.fitBounds(bounds);
      }
    }

    if (data["busstop"] && enable_busstop) {
      var myicon = L.icon({
        iconUrl: 'stop20.png',
        iconAnchor: [12, 12]

      });

      var stop_arr = data["busstop"];
      for (key in stop_arr) {
        var busmarker = new L.Marker([stop_arr[key].lat, stop_arr[key].lng], {
          icon: myicon
        }).addTo(map);;
        busmarker.bindPopup("<a href=prediction.php?busstop=" + stop_arr[key].kod + ">" + stop_arr[key].stop_name + "</a>");
        stop_arr[key].marker = busmarker;
      }
    }

  }
}

var auto_remove = true; //When true, markers for all unreported locs will be removed.
function setMarkers(locObj) {
  if (auto_remove) {
    // а вот экономим ресурсы2
    //Remove markers for all unreported locs, and the corrsponding locations entry.
    $.each(locations, function(key) {
      if (!locObj[key]) {
        if (locations[key].marker !== null && locations[key].marker !== undefined) {
          if (map.hasLayer(locations[key].marker))
            map.removeLayer(locations[key].marker);
          locations[key].marker = null;
        }
        //map.removeLayer(locations[key].arrow);}
        delete locations[key];
      }
    });
  }
  // тут все на буржуйском.и так понятно
  $.each(locObj, function(key, loc) {
    if (!locations[key] && loc.lat !== undefined && loc.lng !== undefined && loc.marsh !== undefined && loc.marshnum != undefined) {
      //Marker has not yet been made (and there's enough data to create one).
      //Remember loc in the `locations` so its info can be displayed and so its marker can be deleted.
      locations[key] = loc;
      //	if(!marker_arr[loc.marsh])
      //addNewMarshMarker(loc.marsh,loc.marshnum);
    } else if (locations[key] && loc.remove) {
      //Remove element from `locations`
      if (locations[key].marker !== null && locations[key].marker !== undefined) {
        if (map.hasLayer(locations[key].marker))
          map.removeLayer(locations[key].marker);
        locations[key].marker = null;
      }
      delete locations[key];
    } else if (locations[key]) {
      //Update the previous data object with the latest data.

      if (loc.lat !== undefined && loc.lng !== undefined) {

        // Лень? почему не вращать если меньше 10 градусов?
        // и где Math.abs...
        // вообще непонятно
        if (loc.angle - locations[key].angle > 10 || locations[key] - loc.angle > 10) {
          if (map.hasLayer(locations[key].marker))
            map.removeLayer(locations[key].marker);
          locations[key].marker = null;
          locations[key].marker = new L.Marker([loc.lat, loc.lng], {
            icon: getCdsMarker(loc.marsh, loc.marshnum, loc.angle),
            opacity: 1
          });
          locations[key].marker.addTo(map);
        }
        $.extend(locations[key], loc);
        if (locations[key].marker)
          locations[key].marker.bindPopup(loc.info);

      }

    }
  });
  //showMarshMarkers();
  // и вот сейчас только перерисуем.
  // итого 1 массив, 2 прохода
  // или даже 2 массива, 3 прохода
  // хотя есть мнение что хватит 1 прохода для обновления
  // ну и наверно еще 1 проход для зачистки
  updateMarkersOnMap();
  //map.setView(map.getCenter());
  var mapdiv = document.getElementById("map");
  mapdiv.style.height = (mapdiv.clientHeight - 10) + "px";
  mapdiv.style.height = (mapdiv.clientHeight + 10) + "px";
  map.invalidateSize();
}

var ajaxObj = { //Object to save cluttering the namespace.
  options: {
    // УУУУУУУУУУУУУУУУУу. секретность!!
    // url: "31912thosie7dai7o24021ooth3iisheis11875au0raith7xey6742ejo8cohtaiw7609831912.php?marsh="+marsh,
    url: "31912thosie7dai7o24021ooth3iisheis11875au0raith7xey6742ejo8cohtaiw7609831912.php?marsh=" + marsh,
    //	url: "31912thosie7dai7o24021ooth3iisheis11875au0raith7xey6742ejo8cohtaiw7609831912.php?marsh="+marsh,
    dataType: "json" //The type of data tp be returned by the server.
  },
  delay: 20000, //(milliseconds) the interval between successive gets.
  first: 0,
  errorCount: 0, //running total of ajax errors.
  errorThreshold: 5, //the number of ajax errors beyond which the get cycle should cease.
  ticker: null, //setTimeout reference - allows the get cycle to be cancelled with clearTimeout(ajaxObj.ticker);
  get: function() { //a function which initiates
    // а вот это красиво
    if (ajaxObj.errorCount < ajaxObj.errorThreshold && ajaxObj.first == 0) {
      ajaxObj.ticker = setTimeout(getMarkerData, 500);
      ajaxObj.first = 1;
    } else {
      ajaxObj.ticker = setTimeout(getMarkerData, ajaxObj.delay);
    }
  },
  fail: function(jqXHR, textStatus, errorThrown) {
    console.log(errorThrown);
    ajaxObj.errorCount++;
  }
};

//Ajax master routine
function getMarkerData() {
  $.ajax(ajaxObj.options)
    .done(setMarkers) //fires when ajax returns successfully
    .fail(ajaxObj.fail) //fires when an ajax error occurs
    .always(ajaxObj.get); //fires after ajax success or ajax error
}

ajaxObj.get(); //Start the get cycle.
function showMarshMarkers() {
  var i = 0;
  for (var key in marker_arr) {
    // непонятно от слова совсем. понятно что вроде бы там пусто
    // но просто интересно что это такое планировалось
    var marker1 = new L.Marker([58.4 + i / 100, 49.4 + i / 100], {
      icon: marker_arr[key],
      opacity: 1
    }).addTo(map);
    i++;
  }
}
