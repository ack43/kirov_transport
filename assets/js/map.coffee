# script = document.createElement('script')
# script.src = "https://m.cdsvyatka.com/dist/leaflet.js"
# document.head.appendChild(script)
#
# script = document.createElement('script')
# script.src = "https://rawgit.com/ewoken/Leaflet.MovingMarker/master/MovingMarker.js"
# document.head.appendChild(script)


map = null
busstops = {}
routes = {}

initMap = ->
  map = L.map 'map', {
    zoomControl: false
  }
  map.setView([58.600060, 49.665317], 13)
  map.locate({
    setView: false
  })
  L.control.zoom(
    position: 'topright'
  ).addTo(map)
  L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 20
  }).addTo(map)
  # button = new L.Control.Button({
  #   position: 'topright',
  # }).addTo(map)




ready = ->
  if map_tag = document.querySelector("#map")
    initMap()
    getBusstops()
    getRoutes()
    setInterval(getRoutes, 1000)


busstopIcon = new (L.icon)({iconUrl: "https://m.cdsvyatka.com/stop20.png"})
getBusstops = ->
  fetch "/api/json/busstops"#, {credentials: 'include'}
  .then (resp)-> resp.json()
  .then (data)->
    for busstop in data
      busstopMarker = new (L.Marker)([busstop.lat, busstop.lon || busstop.lng], {
        icon: busstopIcon
        iconAnchor: [12, 12]
      }).addTo(map)
      busstopMarker.bindPopup("<span>#{busstop.stop_name}</span>")
      busstop.marker = busstopMarker
      busstops[busstop.kod] = busstop
# getBusstops()

busIcon = new (L.icon)({iconUrl: "https://m.cdsvyatka.com/stop20.png"})
getRoutes = ->
  fetch "/api/json/locations"#, {credentials: 'include'}
  .then (resp)-> resp.json()
  .then (data)->
    for route in data
      old_route = routes[route.id] || {}
      old_loc = old_route.last_location || {}

      loc = route.last_location || {}
      if (old_loc.lat != loc.lat) or ((old_loc.lon || old_loc.lng) != (loc.lon || loc.lng))
        # map.removeLayer(marker) if map.hasLayer(marker) if marker
        route = Object.assign(old_route || {}, route)
        marker = old_route.marker
        if marker
          # console.log('relocate marker')
          # new_marker = (L.Marker.movingMarker)(
          #   [
          #     [old_loc.lat, old_loc.lon || old_loc.lng],
          #     [loc.lat, loc.lon || loc.lng],
          #     # icon: getCdsMarker(loc.marsh, loc.marshnum, loc.angle)
          #   ], [1000]
          # ).addTo(map)
          # map.removeLayer(marker)
          # marker = new_marker
          marker.setLatLng([loc.lat, loc.lon || loc.lng]);
        else
          # console.log('new marker')
          marker = new (L.Marker)([loc.lat, loc.lon || loc.lng], {
            # icon: getCdsMarker(loc.marsh, loc.marshnum, loc.angle)
            title: loc.marshnum
          }).addTo(map)

        marker.bindPopup("<span>#{loc.marshnum} (#{loc.marsh})</span><br><span>#{route.machine_name}</span><br><span>#{loc.info}</span>")

        route.marker = marker
        routes[route.id] = route

# getRoutes()




document.addEventListener "DOMContentLoaded", ready

# shamelessly stolen
# https://m.cdsvyatka.com/Icon.Canvas.js
# doesnt work
L.Icon.Canvas = (L.Icon.extend)(
  options:
    iconSize: new (L.Point)(20, 20)
    className: 'leaflet-canvas-icon'
  createIcon: ->
    e = document.createElement('canvas')
    @_setIconStyles e, 'icon'
    s = @options.iconSize
    e.width = s.x
    e.height = s.y
    @draw e.getContext('2d'), s.x, s.y
    e
  createShadow: ->
    null
  draw: (canvas, width, height) ->
)
L.Control.Button = L.Control.extend(
  options: position: 'topleft'
  onAdd: (map) ->
    className = 'leaflet-control-button'
    container = L.DomUtil.create('div', className)
    @_on = @options.on
    @_map = map
    @_zoomInButton = @_createButton('�', 'Показать/Скрыть остановки', className, container, @_zoomIn, this)
    container
  onRemove: (map) ->
  _zoomIn: (e) ->
    #this._map.zoomIn(e.shiftKey ? 3 : 1);
    @_on = !@_on
    if @_on
      @_image.setAttribute 'src', 'stop242.png'
      if @options.onFunc
        @options.onFunc.call()
    else
      @_image.setAttribute 'src', 'stop24.png'
      if @options.offFunc
        @options.offFunc.call()
    return
  _createButton: (html, title, className, container, fn, context) ->
    link = L.DomUtil.create('div', className, container)
    #link.innerHTML = html;
    #link.href = '#';
    link.title = title
    @_image = L.DomUtil.create('img', 'leaflet-buttons-control-img', link)
    if @_on
      @_image.setAttribute 'src', 'stop242.png'
    else
      @_image.setAttribute 'src', 'stop24.png'
    stop = L.DomEvent.stopPropagation
    L.DomEvent.on(link, 'click', stop).on(link, 'mousedown', stop).on(link, 'dblclick', stop).on(link, 'click', L.DomEvent.preventDefault).on link, 'click', fn, context
    link
)

L.control.button = (options) ->
  new (L.Control.Button)(options)



getCdsMarker = (marsh, marshnum, angle) ->
  fillcolor = '#F00' # city
  if marsh > 5000
    fillcolor = '#0F0' # trolley
  else
    if marsh > 3000
      fillcolor = '#00F' # country

  new (L.Icon.Canvas)(
    iconSize: new (L.Point)(60, 60)
    iconAnchor: new (L.Point)(30, 30)
  ).draw = (ctx, w, h) ->
    ctx.translate 30, 30
    ctx.rotate angle * Math.PI / 180
    ctx.beginPath()
    ctx.lineWidth = 2
    ctx.strokeStyle = '#FFF'
    ctx.fillStyle = fillcolor
    ctx.arc 0, 0, 14, -60 * Math.PI / 180, 240 * Math.PI / 180, false
    ctx.fill()
    ctx.stroke()
    ctx.closePath()
    ctx.beginPath()
    ctx.moveTo 8, -12
    ctx.lineTo 0, -25
    ctx.lineTo -8, -12
    ctx.fill()
    ctx.stroke()
    ctx.closePath()
    ctx.rotate -angle * Math.PI / 180
    ctx.fillStyle = '#FFF'
    ctx.font = 'bold 16px '
    ctx.fillText marshnum, -8, 4
