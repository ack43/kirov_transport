use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :kirov_transport, KirovTransportWeb.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
# config :kirov_transport, KirovTransport.Repo,
#   adapter: Ecto.Adapters.Postgres,
#   username: "postgres",
#   password: "postgres",
#   database: "kirov_transport_test",
#   hostname: "localhost",
#   pool: Ecto.Adapters.SQL.Sandbox
