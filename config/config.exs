# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :kirov_transport,
  ecto_repos: [KirovTransport.Repo]
# config :kirov_transport, KirovTransport.Repo,
#   # adapter: Mongo.Ecto,
#   database: "kirov_transport",
#   hostname: "localhost"


# Configures the endpoint
config :kirov_transport, KirovTransportWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "/drRrskcIzE6LI1eHgQFncyq5clfLHz3avCOT66wcFt+W7dFFnSLOKL5k4Awzvxi",
  render_errors: [view: KirovTransportWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: KirovTransport.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]


config :torch,
  otp_app: :kirov_transport,
  template_format: "eex" || "slim"

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
