defmodule KirovTransportWeb.CDSVyatka.MachineView do
  use KirovTransportWeb, :view

  def render("index.json", %{machines: machines}) do
    Enum.map(machines, &index_machine_json/1)
  end

  def render("show.json", %{machine: machine}) do
    show_machine_json(machine)
  end



  def machine_json(nil), do: %{}
  def machine_json(machine) when map_size(machine) == 0, do: %{}
  def machine_json(machine) do
    %{
      id: machine.id,
      name: machine.name,
      inserted_at: machine.inserted_at,
      updated_at: machine.updated_at
    }
  end
  def index_machine_json(machine) do
    machine_json(machine)
  end
  def show_machine_json(machine) do
    machine_json(machine)
      # |> Map.put(:days, machine.days)
  end
end
