defmodule KirovTransportWeb.CDSVyatka.RouteView do
  use KirovTransportWeb, :view

  def render("index.json", %{routes: routes}) do
    Enum.map(routes, &index_route_json/1)
  end

  def render("show.json", %{route: route}) do
    show_route_json(route)
  end

  def render("machines.json", %{machines: machines}) do
    (machines || [])
      |> Enum.map(fn(m)->
        machine_day_json(m)
          |> Map.put(:locations, locations_route_json(m.locations))
      end)
  end


  def machine_day_json(nil), do: %{}
  def machine_day_json(machine_day) when map_size(machine_day) == 0, do: %{}
  def machine_day_json(machine_day) do
    %{
      id: machine_day.id,
      machine_id: machine_day.machine_id,
      machine_name: machine_day.machine_name,

      locations: []

      # last_location: location_json(route.last_location),
      # current_location: location_json(route.current_location)

      # inserted_at: route.inserted_at,
      # updated_at: route.updated_at
    }
  end


  def route_json(nil), do: %{}
  def route_json(route) when map_size(route) == 0, do: %{}
  def route_json(route) do
    %{
      id: route.id,
      marsh: route.marsh,
      marshnum: route.marshnum,
      shortname: route.shortname,
      smartstop: route.smartstop,

      inserted_at: route.inserted_at,
      updated_at: route.updated_at
    }
  end
  def index_route_json(route) do
    route_json(route)
  end
  def show_route_json(route) do
    points_json = (route.points || [])
      |> Enum.map(&point_json/1)
    route_json(route)
      |> Map.put(:points, points_json)
  end

  def point_json(nil), do: %{}
  def point_json(point) when map_size(point) == 0, do: %{}
  def point_json(point) do
    %{
      id:  point.id,
      lat: point.lat,
      lon: point.lon,
      num: point.num
    }
  end


  def locations_route_json(locations) do
    locations
      |> Enum.map(&location_json/1)
  end


  def location_json(nil), do: %{}
  def location_json(location) when map_size(location) == 0, do: %{}
  def location_json(location) do
    %{
      id:         location.id,
      marsh:      location.marsh,
      marshnum:   location.marshnum,
      lat:        location.lat,
      lon:        location.lon,
      angle:      location.angle,
      info:       location.info,
      timestamp:  location.timestamp
    }
  end
end
