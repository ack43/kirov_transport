defmodule KirovTransportWeb.CDSVyatka.BusstopView do
  use KirovTransportWeb, :view

  def render("index.json", %{busstops: busstops}) do
    Enum.map(busstops, &index_busstop_json/1)
  end

  def render("show.json", %{busstop: busstop}) do
    show_busstop_json(busstop)
  end



  def busstop_json(nil), do: %{}
  def busstop_json(busstop) when map_size(busstop) == 0, do: %{}
  def busstop_json(busstop) do
    %{
      id: busstop.id,
      stop_name: busstop.stop_name,
      link: busstop.link,

      kod: busstop.kod,
      alt_kod: busstop.alt_kod,

      lat: busstop.lat,
      lon: busstop.lng,
      # lng: busstop.lng,

      # inserted_at: busstop.inserted_at,
      # updated_at: busstop.updated_at
    }
  end
  def index_busstop_json(busstop) do
    busstop_json(busstop)
  end
  def show_busstop_json(busstop) do
    busstop_json(busstop)
  end
end
