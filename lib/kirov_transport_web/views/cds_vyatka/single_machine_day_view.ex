defmodule KirovTransportWeb.CDSVyatka.SingleMachineDayView do
  use KirovTransportWeb, :view

  def render("index.json", %{routes: routes}) do
    Enum.map(routes, &index_route_json/1)
  end

  def render("show.json", %{route: route}) do
    show_route_json(route)
  end


  def route_json(nil), do: %{}
  def route_json(route) when map_size(route) == 0, do: %{}
  def route_json(route) do
    %{
      id: route.id,
      machine_id: route.machine_id,
      machine_name: route.machine_name,

      last_location: location_json(route.last_location),
      # current_location: location_json(route.current_location)

      # inserted_at: route.inserted_at,
      # updated_at: route.updated_at
    }
  end
  def index_route_json(route) do
    route_json(route)
  end
  def show_route_json(nil), do: %{}
  def show_route_json(route) when map_size(route) == 0, do: %{}
  def show_route_json(route) do
    locations_json = (route.locations || [])
      |> Enum.map(&location_json/1)
    route_json(route)
      # |> Map.put(:last_location, location_json(route.last_location))
      |> Map.put(:current_location, location_json(route.current_location))
      |> Map.put(:locations, locations_json)
  end

  def location_json(nil), do: %{}
  def location_json(location) when map_size(location) == 0, do: %{}
  def location_json(location) do
    %{
      id:         location.id,
      marsh:      location.marsh,
      marshnum:   location.marshnum,
      lat:        location.lat,
      lon:        location.lon,
      angle:      location.angle,
      info:       location.info,
      timestamp:  location.timestamp
    }
  end
end
