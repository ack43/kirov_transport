defmodule KirovTransportWeb.RouteResolver do
  alias KirovTransport.CDSVyatka.Route

  def find(%{shortname: shortname}, info), do: all(%{name: shortname}, info)
  def all(%{name: name}, _info) do
    {:ok, Route.all(%{name: name})}
  end

  def all(%{marshnum: marshnum}, _info) do
    {:ok, Route.all(%{marshnum: marshnum})}
  end

  def all(_args, _info) do
    {:ok, Route.all()}
  end




  def find(%{id: id}, _info) do
    case Route.find(id) do
      nil -> {:error, "Route id #{id} not found!"}
      route -> {:ok, route}
    end
  end

  def find(%{shortname: shortname}, info), do: find(%{name: shortname}, info)
  def find(%{name: name}, _info) do
    case Route.Query.by_name(name) do
      nil -> {:error, "Route name #{name} not found!"}
      route -> {:ok, route}
    end
  end

  def find(%{marsh: marsh}, _info) do
    case Route.Query.by_route(marsh) do
      nil -> {:error, "Route marsh #{marsh} not found!"}
      route -> {:ok, route}
    end
  end

  def find(%{marshnum: marshnum}, _info) do
    case Route.Query.by_marshnum(marshnum) do
      nil -> {:error, "Route marshnum #{marshnum} not found!"}
      route -> {:ok, route}
    end
  end

end
