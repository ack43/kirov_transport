defmodule KirovTransportWeb.Schema.Types do
  use Absinthe.Schema.Notation
  use Absinthe.Ecto, repo: KirovTransport.Repo

  object :route do
    field(:id, :id)
    field(:marsh, :string)
    field(:marshnum, :integer)
    field(:shortname, :string)
    field(:smartstop, :boolean)
    # field(:enabled, :boolean)
    field(:points, list_of(:route_point), resolve: assoc(:points))
  end

  object :route_point do
    field(:id, :id)
    field(:lat, :float)
    field(:lon, :float)
    field(:num, :integer)
    # field(:enabled, :boolean)
  end

end
