defmodule KirovTransportWeb.Schema do
  use Absinthe.Schema
  import_types(KirovTransportWeb.Schema.Types)

  query do
    field :routes, list_of(:route) do
      arg(:name, :string)
      arg(:shortname, :string)
      arg(:marshnum, :integer)
      resolve(&KirovTransportWeb.RouteResolver.all/2)
    end

    field :route, type: :route do
      arg(:id, :id)
      arg(:marsh, :string)
      resolve(&KirovTransportWeb.RouteResolver.find/2)
    end

    mutation do
      # field :create_post, type: :blog_post do
      #   arg(:title, non_null(:string))
      #   arg(:body, non_null(:string))
      #   arg(:accounts_user_id, non_null(:id))
      #
      #   resolve(&BlogAppGql.Web.Blog.PostResolver.create/2)
      # end
      #
      # field :update_post, type: :blog_post do
      #   arg(:id, non_null(:id))
      #   arg(:post, :update_post_params)
      #
      #   resolve(&BlogAppGql.Web.Blog.PostResolver.update/2)
      # end
      #
      # field :delete_post, type: :blog_post do
      #   arg(:id, non_null(:id))
      #   resolve(&BlogAppGql.Web.Blog.PostResolver.delete/2)
      # end
    end

  end

end
