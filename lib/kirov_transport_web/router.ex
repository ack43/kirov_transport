defmodule KirovTransportWeb.Router do
  use KirovTransportWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :graphql do
	  # plug KirovTransportWeb.Endpoint
  end


  # Other scopes may use custom stacks.
  scope "/api/json", KirovTransportWeb do
    pipe_through :api

    resources "/routes",    CDSVyatka.RouteController, only: [:index, :show] do
      get "/locations", CDSVyatka.RouteController, :locations
    end
    resources "/locations", CDSVyatka.SingleMachineDayController, only: [:index, :show]
    resources "/machines",  CDSVyatka.MachineController, only: [:index, :show] do
      get "/locations", CDSVyatka.SingleMachineDayController, :show
    end
    resources "/busstops",  CDSVyatka.BusstopController, only: [:index, :show]
  end
  # scope "/api" do
  #   # forward("/graphql",  Absinthe.Plug,          schema: KirovTransportWeb.Schema)
  #   # forward("/graphiql", Absinthe.Plug.GraphiQL, schema: KirovTransportWeb.Schema)
  #   forward "/graphiql", Absinthe.Plug.GraphiQL,
  #     schema: KirovTransportWeb.Schema,
  #     socket: KirovTransportWeb.UserSocket
  # end
  scope "/api" do
    pipe_through(:graphql)

    forward("/graphql", Absinthe.Plug, schema: KirovTransportWeb.Schema)
    forward("/graphiql", Absinthe.Plug.GraphiQL, schema: KirovTransportWeb.Schema)
  end


  scope "/", KirovTransportWeb do
    pipe_through :browser # Use the default browser stack

    resources "/machines",  CDSVyatka.MachineController, only: [:index, :show]
    resources "/routes",    CDSVyatka.RouteController, only: [:index, :show]

    get "/", PageController, :index
  end
end
