defmodule KirovTransportWeb.CDSVyatka.MachineController do
  use KirovTransportWeb, :controller

  def index(%{private: %{phoenix_pipelines: [:browser], phoenix_format: "html"}} = conn, _params) do
    machines = KirovTransport.CDSVyatka.Machine.Query.all
    render conn, "index.html", machines: machines
  end
  def index(conn, _params) do
    machines = KirovTransport.CDSVyatka.Machine.Query.all
    render conn, "index.json", machines: machines
  end


  def show(%{private: %{phoenix_pipelines: [:browser], phoenix_format: "html"}} = conn, %{"id" => id} = _params) do
    machine = KirovTransport.CDSVyatka.Machine.find(id)
    render conn, "show.html", machine: machine
  end
  def show(conn, %{"id" => id} = _params) do
    machine = KirovTransport.CDSVyatka.Machine.find(id)
    render conn, "show.json", machine: machine
  end
end
