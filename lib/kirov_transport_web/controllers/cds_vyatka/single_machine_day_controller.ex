defmodule KirovTransportWeb.CDSVyatka.SingleMachineDayController do
  use KirovTransportWeb, :controller


  def index(conn, %{"timestamp" => timestamp} = _params) do
    timestamp = Timex.parse!(timestamp, "{ISO:Extended}")
    routes = KirovTransport.CDSVyatka.SingleMachineDayWithoutLocations.Query.by_date!(timestamp)
      # |> KirovTransport.CDSVyatka.SingleMachineDayWithoutLocations.get_location_for(timestamp)
    render conn, "index.json", routes: routes
  end
  def index(conn, _params) do
    routes = KirovTransport.CDSVyatka.SingleMachineDayWithoutLocations.Query.by_date!
    render conn, "index.json", routes: routes
  end



  def show(conn, %{"timestamp" => timestamp, "machine_id" => machine_id} = _params) do
    timestamp = Timex.parse!(timestamp, "{ISO:Extended}")
    route = KirovTransport.CDSVyatka.SingleMachineDay.Query.for_date_and_machine(timestamp, machine_id)
      |> KirovTransport.CDSVyatka.SingleMachineDay.get_location_for(timestamp)
    render conn, "show.json", route: route
  end
  def show(conn, %{"machine_id" => machine_id} = _params) do
    date = Timex.parse!("05.08.2018", "{D}.{M}.{YYYY}")
    route = KirovTransport.CDSVyatka.SingleMachineDay.Query.for_date_and_machine(date, machine_id)
    render conn, "show.json", route: route
  end
  def show(conn, %{"timestamp" => timestamp, "id" => id} = _params) do
    timestamp = Timex.parse!(timestamp, "{ISO:Extended}")
    route = KirovTransport.CDSVyatka.SingleMachineDay.find(id)
      |> KirovTransport.CDSVyatka.SingleMachineDay.get_location_for(timestamp)
    render conn, "show.json", route: route
  end
  def show(conn, %{"id" => id} = _params) do
    route = KirovTransport.CDSVyatka.SingleMachineDay.find(id)
    render conn, "show.json", route: route
  end
end
