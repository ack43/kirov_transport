defmodule KirovTransportWeb.CDSVyatka.RouteController do
  use KirovTransportWeb, :controller


  def index(%{private: %{phoenix_pipelines: [:browser], phoenix_format: "html"}} = conn, _params) do
    routes = KirovTransport.CDSVyatka.Route.Query.all
    render conn, "index.html", routes: routes
  end
  def index(conn, _params) do
    routes = KirovTransport.CDSVyatka.Route.Query.all
    render conn, "index.json", routes: routes
  end


  def show(%{private: %{phoenix_pipelines: [:browser], phoenix_format: "html"}} = conn,
            %{"id" => _id} = params) do
      # date = Timex.parse!("09.08.2018", "{D}.{M}.{YYYY}")
      params = Map.put(params, "date", "09.08.2018")
      show(conn, params)
  end
  def show(%{private: %{phoenix_pipelines: [:browser], phoenix_format: "html"}} = conn,
            %{"id" => id, "date" => date} = _params) do
    date = Timex.parse!(date, "{D}.{M}.{YYYY}")
    route = KirovTransport.CDSVyatka.Route.find(id)
    # route = KirovTransport.CDSVyatka.Route.get_by_marsh(id)
    # machines = KirovTransport.CDSVyatka.Route.Query.get_machines(route, date)
    # render conn, "show.html", route: route, machines: machines
    render conn, "show.html", route: route, date: date
  end
  def show(conn, %{"id" => id} = _params) do
    route = KirovTransport.CDSVyatka.Route.find(id)
    render conn, "show.json", route: route
  end


  def machines(conn, %{"route" => _route, "route_id" => _route_id} = params) do
    # date = Timex.parse!("09.08.2018", "{D}.{M}.{YYYY}")
    params = Map.put(params, "date", "09.08.2018")
    machines(conn, params)
  end
  def machines(conn, %{"route_id" => route_id, "date" => date} = _params) do
    date = Timex.parse!(date, "{D}.{M}.{YYYY}")
    route = KirovTransport.CDSVyatka.Route.find(route_id)
    # date = Timex.parse!("09.08.2018", "{D}.{M}.{YYYY}")
    machines = KirovTransport.CDSVyatka.Route.Query.get_machines(route, date)
    render conn, "machines.json", machines: machines
  end
end
