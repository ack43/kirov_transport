defmodule KirovTransportWeb.CDSVyatka.BusstopController do
  use KirovTransportWeb, :controller

  def index(conn, _params) do
    busstops = KirovTransport.CDSVyatka.Busstop.Query.all
    render conn, "index.json", busstops: busstops
  end

  def show(conn, %{"id" => id} = _params) do
    busstop = KirovTransport.CDSVyatka.Busstop.find(id)
    render conn, "show.json", busstop: busstop
  end
end
