defmodule KirovTransportWeb.PageController do
  use KirovTransportWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
