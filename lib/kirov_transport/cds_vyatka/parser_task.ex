defmodule KirovTransport.CDSVyatka.ParserTask do
  use GenServer
  alias KirovTransport.CDSVyatka.Parser, as: Parser

  def start_link do
    # IO.inspect "start_link"
    GenServer.start_link(__MODULE__, %{})
  end

  def init(state) do
    # IO.inspect "init"
    Task.async(fn->
      schedule_work(:get_busstops)
    end) |> Task.await
    Task.async(fn->
      schedule_work(:get_routes)
    end) |> Task.await
    # schedule_work(:get_route_points)
    schedule_work()
    {:ok, state}
  end

  defp schedule_work(task \\ :get_current_routes) do
    # IO.inspect "schedule_work"
    # IO.inspect task
    pauses = %{
      get_current_routes: 5 * 1000, # 5 seconds
      get_routes:         0,
      get_route_points:   0,
      get_busstops:       0
    }
    if Map.has_key?(pauses, task) do
      Process.send_after(self(), task, pauses[task])
    end
  end

  # Callbacks

  def handle_info(:get_current_routes, state) do
    # IO.inspect "handle_info get_current_routes"
    # Do the desired work here
    process(:get_current_routes, state)
    schedule_work() # Reschedule once more
    {:noreply, state}
  end
  def handle_info(:get_routes, state) do
    # IO.inspect "handle_info get_routes"
    # Do the desired work here
    process(:get_routes, state)
    # schedule_work() # Reschedule once more
    {:noreply, state}
  end
  def handle_info(:get_route_points, state) do
    # IO.inspect "handle_info get_route_points"
    # Do the desired work here
    process(:get_route_points, state)
    # schedule_work() # Reschedule once more
    {:noreply, state}
  end
  def handle_info(:get_busstops, state) do
    # IO.inspect "handle_info get_busstops"
    # Do the desired work here
    process(:get_busstops, state)
    # schedule_work() # Reschedule once more
    {:noreply, state}
  end


  def process(:get_current_routes, state) do
    # IO.inspect "get_current_routes"
    # IO.inspect Parser.get_current_routes(100)
    Parser.get_current_routes(200)
    state
  end
  def process(:get_routes, state) do
    # IO.inspect "get_routes"
    # IO.inspect Parser.get_routes
    Parser.get_routes
    state
  end
  def process(:get_route_points, state) do
    # IO.inspect "get_route_points"
    # IO.inspect Parser.get_route_points
    Parser.get_route_points
    state
  end
  def process(:get_busstops, state) do
    # IO.inspect "get_busstops"
    # IO.inspect Parser.get_busstops
    Parser.get_busstops
    state
  end

end
