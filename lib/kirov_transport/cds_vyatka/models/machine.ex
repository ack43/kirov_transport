defmodule KirovTransport.CDSVyatka.Machine do
  use Ecto.Schema

  @primary_key {:id, :binary_id, autogenerate: true}
  schema "machines" do
    field :name, :string
    # field :marsh, :integer
    # field :marshnum, :integer

    has_many :days, KirovTransport.CDSVyatka.SingleMachineDay

    field :enabled, :boolean, default: true
    timestamps()
  end



  import Ecto.Changeset
  def changeset(%__MODULE__{} = machine, params \\ %{}) do
    machine
      |> cast(params, [:name, :enabled])
      |> validate_required([:name])
  end
  def changeset_new_machine(%__MODULE__{} = machine, params \\ %{}) do
    # params = scrub_params(params)  # change "" to nil
    machine
      # |> cast(params, [:name, :marsh, :marshnum, :enabled])
      |> cast(params, [:name, :enabled])
      |> validate_required([:name])
  end

  def count, do: __MODULE__.Query.count
  def find(id), do: KirovTransport.Repo.get!(__MODULE__, id)
  def get_by_name(name), do: __MODULE__.Query.by_name(name)

  def get_by_name_or_create(name, params) do
    machine = get_by_name(name)
    unless machine do
      machine = changeset_new_machine(%__MODULE__{}, params)
        |> KirovTransport.Repo.insert!
    end
    machine
  end


  defmodule Query do
    import Ecto.Query

    def by_name(name) do
      # query = from m in "machines",
      query = from m in KirovTransport.CDSVyatka.Machine,
            where: m.name == ^name,
            select: m
      KirovTransport.Repo.one(query)
    end

    def count, do: KirovTransport.Repo.aggregate(KirovTransport.CDSVyatka.Machine, :count, :id)
    def all, do: KirovTransport.Repo.all(KirovTransport.CDSVyatka.Machine)
  end
end
