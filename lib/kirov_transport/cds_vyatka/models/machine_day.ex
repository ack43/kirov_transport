defmodule KirovTransport.CDSVyatka.MachineDay do
  use Ecto.Schema

  embedded_schema do
    belongs_to :machine, KirovTransport.CDSVyatka.Machine
    field :machine_name, :string
    field :marsh, :string
    field :marshnum, :integer

    field :enabled, :boolean, default: true

    embeds_many :locations, KirovTransport.CDSVyatka.MachineLocation
  end

  import Ecto.Changeset
  def changeset(%__MODULE__{} = machine_day, params \\ %{}) do
    machine_day
      |> KirovTransport.Repo.preload(:machine)
      |> cast(params, [:machine_name, :marsh, :marshnum, :enabled])
      |> cast_assoc(:machine)
      |> validate_required([:machine_name, :marsh])
  end
  def changeset_new_machine_day(%__MODULE__{} = machine_day, params \\ %{}) do
    machine_day
      |> cast(params, [:machine_name, :marsh, :marshnum, :enabled])
      |> cast_assoc(:machine)
      |> validate_required([:machine_name, :marsh])
  end


  def add_machine_location(machine_day, machine_location) do
    KirovTransport.CDSVyatka.CalendarDay.Query.by_machine_day(machine_day.id)
      |> add_machine_location(machine_day, machine_location)
  end
  def add_machine_location(calendar_day, machine_day, machine_location) do
    KirovTransport.CDSVyatka.CalendarDay.add_machine_location(calendar_day, machine_day, machine_location)
  end

  # def count, do: KirovTransport.CDSVyatka.Machine.Query.count


  # defmodule Query do
  #   import Ecto.Query
  #
  #   def by_name(name) do
  #     # query = from r in KirovTransport.CDSVyatka.Machine,
  #     query = from m in "machines",
  #           where: m.name == ^name,
  #           select: m
  #     KirovTransport.Repo.one(query)
  #   end
  #
  #   def count, do: KirovTransport.Repo.aggregate(KirovTransport.CDSVyatka.Machine, :count, :id)
  # end
end
