defmodule KirovTransport.CDSVyatka.RoutePoint do
  use Ecto.Schema

  embedded_schema do
    field :lat, :float
    field :lon, :float
    field :num, :integer

    field :enabled, :boolean, default: true
  end

  import Ecto.Changeset
  def changeset(route_point, params \\ %{}) do
    route_point
      |> cast(params, [:lat, :lon, :num])
      |> validate_required([:lat, :lon, :num])
  end
  def changeset_new_route_point(route_point, params \\ %{}) do
    params = params
      |> Map.put_new("lon", params[:lng] || params["lng"])

    # params = scrub_params(params)  # change "" to nil
    # route_point
    #   |> cast(params, [:lat, :lon, :num])
    #   |> validate_required([:lat, :lon, :num])
    changeset(route_point, params)
  end

  # def count, do: KirovTransport.CDSVyatka.RoutePoint.Query.count


  # defmodule Query do
  #   import Ecto.Query
  #
  #   def by_route(route) do
  #     # query = from r in KirovTransport.CDSVyatka.RoutePoint,
  #     query = from r in "routes",
  #           where: r.marsh == ^route,
  #           select: r
  #     KirovTransport.Repo.one(query)
  #   end
  #
  #   def count, do: KirovTransport.Repo.aggregate(KirovTransport.CDSVyatka.RoutePoint, :count, :id)
  # end
end
