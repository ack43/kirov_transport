defmodule KirovTransport.CDSVyatka.RouteBusstop do
  use Ecto.Schema

  # alias __MODULE__.Query, as: Query


  embedded_schema do
    # belongs_to :busstop, KirovTransport.CDSVyatka.Busstop
    field :busstop_id, :string
    field :busstop, :map, virtual: true

    # belongs_to :alt_busstop, KirovTransport.CDSVyatka.Busstop
    field :alt_busstop_id, :string
    field :alt_busstop, :map, virtual: true

    field :kod, :string
    field :alt_kod, :string
    field :location_id, :integer

    field :lat, :float
    field :lng, :float
    field :link, :string
    field :stop_name, :string
    field :stop_name_synonyms, {:array, :string}, default: []

    field :enabled, :boolean, default: true
  end

  import Ecto.Changeset
  def changeset(route_busstop, params \\ %{}) do
    # params = scrub_params(params)  # change "" to nil
    route_busstop_as_map = Map.from_struct(route_busstop)
    route_busstop
      |> cast(params, [:kod, :lat, :lng, :link, :stop_name, :stop_name_synonyms, :enabled, :location_id])
      |> cast(route_busstop_as_map, [:busstop_id, :alt_busstop_id])
      |> validate_required([:kod, :lat, :lng, :stop_name])
  end
  def changeset_new_route_busstop(route_busstop, params \\ %{}) do
    # params = scrub_params(params)  # change "" to nil
    route_busstop
      |> cast(params, [:kod, :lat, :lng, :link, :stop_name, :stop_name_synonyms, :enabled, :location_id])
      |> validate_required([:kod, :lat, :lng, :stop_name])
  end
  def changeset_from_busstop(route_busstop, params \\ %{}) do
    # params = scrub_params(params)  # change "" to nil
    route_busstop
      |> cast(params, [:kod, :alt_kod, :lat, :lng, :link, :stop_name, :stop_name_synonyms, :enabled, :busstop_id, :alt_busstop_id, :location_id])
      |> validate_required([:kod, :lat, :lng, :stop_name])
  end

  def update_from_busstop(route_busstop) do
    busstop = KirovTransport.CDSVyatka.Busstop.find(route_busstop.busstop_id)
    if busstop do
      busstop_params = busstop
        |> Map.from_struct
        # |> Map.put(:busstop_id, busstop.id)
        # |> Map.put(:alt_busstop_id, busstop.alt_id)
      route_busstop
        |> changeset(busstop_params)
    end
  end

  # def count, do: Query.count
  # def find(id), do: KirovTransport.Repo.get!(__MODULE__, id)
  # def delete_all, do: KirovTransport.Repo.delete_all(__MODULE__)

  # def similar_busstop(route_busstop) do
  #   Query.by_name(route_busstop.stop_name)
  # end

  # def get_and_similar(kod) do
  #   # gracefully
  #   # similar = Query.by_kod(kod).stop_name
  #   #   |> Query.by_name
  #
  #   bs = Query.by_kod(kod)
  #   if bs, do: similar = Query.by_name(bs.stop_name)
  #   similar || []
  # end




  # defmodule Query do
  #   import Ecto.Query
  #
  #   alias KirovTransport.CDSVyatka.Busstop, as: Busstop
  #
  #   def by_kod(kod) do
  #     # # query = from b in Busstop,
  #     # query = from b in "busstops",
  #     #   where: b.kod == ^kod,
  #     #   select: b
  #     # KirovTransport.Repo.one(query)
  #     KirovTransport.Repo.get_by(Busstop, %{kod: to_string(kod)})
  #   end
  #
  #   def by_alt_kod(alt_kod) do
  #     # # query = from b in Busstop,
  #     # query = from b in "busstops",
  #     #   where: b.alt_kod == ^alt_kod,
  #     #   select: b
  #     # KirovTransport.Repo.one(query)
  #     KirovTransport.Repo.get_by(Busstop, %{alt_kod: to_string(alt_kod)})
  #   end
  #
  #   def by_any_kod(any_kod) do
  #     # query = from b in Busstop,
  #     # # query = from b in "busstops",
  #     #   where: b.kod == ^to_string(any_kod) or b.alt_kod == ^to_string(any_kod),
  #     #   select: b
  #     # KirovTransport.Repo.all(query)
  #     KirovTransport.Repo.all(Busstop, %{alt_kod: any_kod, kod: any_kod})
  #   end
  #
  #
  #   def by_name(name) do
  #     # query = from b in KirovTransport.CDSVyatka.Route,
  #     query = from b in "busstops",
  #       where: b.stop_name == ^name,
  #       select: b
  #     KirovTransport.Repo.all(query)
  #     # KirovTransport.Repo.all(Busstop, %{stop_name: to_string(name)})
  #   end
  #
  #   def count, do: KirovTransport.Repo.aggregate(Busstop, :count, :id)
  #   def all, do: KirovTransport.Repo.all(KirovTransport.CDSVyatka.Busstop)
  # end

end
