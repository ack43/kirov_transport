defmodule KirovTransport.CDSVyatka.SingleMachineLocation do
  use Ecto.Schema

  # @primary_key {:id, :binary_id, autogenerate: true}
  embedded_schema do
    field :marsh, :string
    field :marshnum, :integer

    field :lat, :float
    field :lon, :float
    field :angle, :float
    field :info, :string
    field :timestamp, Ecto.DateTime
  end

  import Ecto.Changeset
  def changeset(%__MODULE__{} = machine_location, params \\ %{}) do
    machine_location
      |> cast(params, [:marsh, :marshnum, :lat, :lon, :angle, :info, :timestamp])
      |> validate_required([:marsh, :lat, :lon, :timestamp])
  end
  def changeset_new_machine_location(%__MODULE__{} = machine_location, params \\ %{}) do
    # params = scrub_params(params)  # change "" to nil
    machine_location
      |> cast(params, [:marsh, :marshnum, :lat, :lon, :angle, :info, :timestamp])
      |> validate_required([:marsh, :lat, :lon, :timestamp])
  end

  # def count, do: KirovTransport.CDSVyatka.MachineLocation.Query.count


  # defmodule Query do
  #   import Ecto.Query
  #
  #   def by_name(name) do
  #     # query = from r in KirovTransport.CDSVyatka.Machine,
  #     query = from m in "machines",
  #           where: m.name == ^name,
  #           select: m
  #     KirovTransport.Repo.one(query)
  #   end
  #
  #   def count, do: KirovTransport.Repo.aggregate(KirovTransport.CDSVyatka.MachineLocation, :count, :id)
  # end
end
