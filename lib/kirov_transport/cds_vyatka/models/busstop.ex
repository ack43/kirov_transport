defmodule KirovTransport.CDSVyatka.Busstop do
  use Ecto.Schema

  alias __MODULE__.Query, as: Query


  @primary_key {:id, :binary_id, autogenerate: true}
  schema "busstops" do
    field :kod, :string
    field :alt_kod, :string
    field :location_id, :integer

    field :lat, :float
    field :lng, :float
    field :link, :string
    field :stop_name, :string
    field :stop_name_synonyms, {:array, :string}, default: []

    field :enabled, :boolean, default: true
  end

  import Ecto.Changeset
  def changeset_new_busstop(busstop, params \\ %{}) do
    # params = scrub_params(params)  # change "" to nil
    busstop
      |> cast(params, [:kod, :lat, :lng, :link, :stop_name, :enabled, :stop_name_synonyms, :location_id])
      |> validate_required([:kod, :lat, :lng, :stop_name])
  end

  def count, do: Query.count
  def find(id), do: KirovTransport.Repo.get!(__MODULE__, id)
  def delete_all, do: KirovTransport.Repo.delete_all(__MODULE__)

  def similar_busstop(busstop) do
    Query.by_name(busstop.stop_name)
  end

  def get_and_similar(kod) do
    # gracefully
    # similar = Query.by_kod(kod).stop_name
    #   |> Query.by_name

    bs = Query.by_kod(kod)
    if bs, do: similar = Query.by_name(bs.stop_name)
    similar || []
  end


  def set_location_ids do
    Query.all
      |> Enum.group_by(&(String.trim(&1.stop_name)))
      |> Enum.with_index(0)
      |> Enum.map(fn({{_name, stops}, location_id})->
        Enum.map(stops, fn(bs)->
          change(bs, %{location_id: location_id})
            |> KirovTransport.Repo.update!
        end)
      end)
  end




  defmodule Query do
    import Ecto.Query

    alias KirovTransport.CDSVyatka.Busstop, as: Busstop

    def by_kod(kod) do
      # # query = from b in Busstop,
      # query = from b in "busstops",
      #   where: b.kod == ^kod,
      #   select: b
      # KirovTransport.Repo.one(query)
      KirovTransport.Repo.get_by(Busstop, %{kod: to_string(kod)})
    end

    def by_alt_kod(alt_kod) do
      # # query = from b in Busstop,
      # query = from b in "busstops",
      #   where: b.alt_kod == ^alt_kod,
      #   select: b
      # KirovTransport.Repo.one(query)
      KirovTransport.Repo.get_by(Busstop, %{alt_kod: to_string(alt_kod)})
    end

    def by_any_kod(any_kod) do
      any_kod = to_string(any_kod)
      query = from b in Busstop,
      # query = from b in "busstops",
        where: b.kod == ^to_string(any_kod) or b.alt_kod == ^to_string(any_kod),
        select: b
      KirovTransport.Repo.all(query)
      # KirovTransport.Repo.get_by(Busstop, %{[alt_kod: any_kod], kod: any_kod})
    end


    def by_name(name) do
      regex_name = Mongo.Ecto.Helpers.regex(name, "i")
      # query = from b in KirovTransport.CDSVyatka.Route,
      query = from b in "busstops",
        where: b.stop_name == ^name or fragment("stop_name_synonyms": ^regex_name),
        select: b
      KirovTransport.Repo.all(query)
      # KirovTransport.Repo.all(Busstop, %{stop_name: to_string(name)})
    end

    def by_location_id(%Busstop{location_id: location_id} = _bs), do: by_location_id(location_id)
    def by_location_id(location_id) do
      query = from b in "busstops",
        where: b.location_id == ^location_id,
        select: b
      KirovTransport.Repo.all(query)
    end

    def count, do: KirovTransport.Repo.aggregate(Busstop, :count, :id)
    def all, do: KirovTransport.Repo.all(KirovTransport.CDSVyatka.Busstop)
  end

end
