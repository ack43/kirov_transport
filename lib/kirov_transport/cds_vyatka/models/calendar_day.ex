defmodule KirovTransport.CDSVyatka.CalendarDay do
  use Ecto.Schema

  @primary_key {:id, :binary_id, autogenerate: true}
  schema "calendar_days" do
    field :date, :date
    embeds_many :machines, KirovTransport.CDSVyatka.MachineDay

    field :enabled, :boolean, default: true
  end

  import Ecto.Changeset
  def changeset(%__MODULE__{} = calendar_day, params \\ %{}) do
    calendar_day
      |> cast(params, [:date, :enabled])
      |> validate_required([:date])
  end
  def changeset_new_calendar_day(%__MODULE__{} = calendar_day, params \\ %{}) do
    # IO.inspect calendar_day
    # IO.inspect params
    # params = scrub_params(params)  # change "" to nil
    calendar_day
      |> cast(params, [:date, :enabled])
      |> validate_required([:date])
  end

  def create(params) do
    %__MODULE__{}
      |> changeset_new_calendar_day(params)
      |> KirovTransport.Repo.insert!
  end

  def count, do: KirovTransport.CDSVyatka.CalendarDay.Query.count


  def add_machine(calendar_day, machine_day) do
    # calendar_day.machines = [calendar_day.machines | machine_day]
    machines = calendar_day.machines ++ machine_day
    calendar_day
      |> change
      |> put_embed(:machines, machines)
      # |> KirovTransport.Repo.update!
    # Mongo.update_one(__MODULE__, "calendar_days", %{"_id": calendar_day.id},
    #   %{"$addToSet": %{"machines": machine_day}}
    # )
    # calendar_day.machines = calendar_day.machines ++ [machine_day]
    # calendar_day
  end
  def add_machine_location(calendar_day, machine_day, machine_location) do
    Mongo.update_one(__MODULE__, "calendar_days", %{"_id": calendar_day.id, "machines.id": machine_day.id},
      %{"$addToSet": %{"machines.$.locations": machine_location}}
    )
    # machine_day.locations = machine_day.locations ++ [machine_location]
    calendar_day
  end


  defmodule Query do
    import Ecto.Query

    def by_date(date) do
      # query = from cd in "calendar_days",
      query = from cd in KirovTransport.CDSVyatka.CalendarDay,
            where: cd.date == ^date,
            select: cd
      KirovTransport.Repo.one(query)
    end

    def by_machine_day(machine_day) when is_map(machine_day), do: by_machine_day(machine_day.id)
    def by_machine_day(machine_day_id) do
      # query = from cd in KirovTransport.CDSVyatka.CalendarDay,
      #       where: cd.machines.id == ^machine_day_id,
      #       select: cd
      # KirovTransport.Repo.one(query)

      Mongo.find_one(KirovTransport.CDSVyatka.CalendarDay, "calendar_days", %{"machines.id": machine_day_id})
      # KirovTransport.Repo.get_by(KirovTransport.CDSVyatka.CalendarDay, %{"machines.id": machine_day_id})
    end

    def count, do: KirovTransport.Repo.aggregate(KirovTransport.CDSVyatka.CalendarDay, :count, :id)
  end
end
