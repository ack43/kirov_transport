defmodule KirovTransport.CDSVyatka.SingleMachineDay do
  use Ecto.Schema

  @derive [Poison.Encoder]

  @primary_key {:id, :binary_id, autogenerate: true}
  schema "machine_days" do
    field :date, :date

    # belongs_to :machine, KirovTransport.CDSVyatka.Machine
    field :machine_id, :string

    field :machine, :map, virtual: true
    # field :last_location, :map, virtual: true
    field :current_location, :map, virtual: true

    field :machine_name, :string

    field :enabled, :boolean, default: true

    # field :array1, {:array, :map}#, virtual: true

    #############################################
    # field :last_location, :map
    # field :locations, {:array, :map}
    #############################################
    embeds_one :last_location, KirovTransport.CDSVyatka.SingleMachineLocation, on_replace: :delete
    embeds_many :locations, KirovTransport.CDSVyatka.SingleMachineLocation, on_replace: :delete
  end



  import Ecto.Changeset
  def changeset(%__MODULE__{} = machine_day, params \\ %{}) do
    machine_day = get_machine(machine_day)
    machine_day
      # |> KirovTransport.Repo.preload(:machine)
      |> cast(params, [:machine_name, :date, :machine_id])
      |> cast(machine_day, :machine)
      |> validate_required([:machine_name, :date, :machine_id])
  end
  def changeset_new_machine_day(%__MODULE__{} = machine_day, params \\ %{}) do
    machine_day
      |> cast(params, [:machine_name, :date, :machine_id])
      # |> cast_assoc(:machine)
      |> validate_required([:machine_name, :date, :machine_id])
  end


  # def add_machine_location(machine_day, machine_location) do
  #   # KirovTransport.CDSVyatka.CalendarDay.Query.by_machine_day(machine_day.id)
  #   #   |> add_machine_location(machine_day, machine_location)
  # end


  def count, do: KirovTransport.CDSVyatka.SingleMachineDay.Query.count
  def delete_all, do: KirovTransport.Repo.delete_all(__MODULE__)

  def find(id), do: KirovTransport.Repo.get!(__MODULE__, id)
  # def find(id), do: get_last_location(KirovTransport.Repo.get!(__MODULE__, id))
  def find_with_machine(id), do: get_machine(find(id))

  def get_machine(%__MODULE__{machine_id: nil, machine: nil} = machine_day), do: machine_day
  def get_machine(%__MODULE__{machine_id: _machine_id, machine: machine} = machine_day) when is_map(machine), do: machine_day
  def get_machine(%__MODULE__{machine_id: machine_id, machine: nil} = machine_day) when is_binary(machine_id) do
    machine_day
      |> Map.put(:machine, KirovTransport.CDSVyatka.Machine.find(machine_id))
  end

  def get_last_location(%__MODULE__{locations: nil, last_location: nil} = machine_day), do: machine_day
  def get_last_location(%__MODULE__{locations: locations, last_location: %{}} = machine_day) when is_list(locations) do
    machine_day
      |> Map.put(:last_location, nil)
      |> get_last_location
  end
  def get_last_location(%__MODULE__{locations: _locations, last_location: last_location} = machine_day) when is_map(last_location), do: machine_day
  def get_last_location(%__MODULE__{locations: locations, last_location: nil} = machine_day) when is_list(locations) do
    last_location = locations
      # |> Enum.sort_by(:timestamp)
      |> Enum.max_by(fn(l)-> l.timestamp end)
      # |> Enum.at(0)
    machine_day
      |> Map.put(:last_location, last_location)
  end
  def get_last_location(machine_days) when is_list(machine_days) do
    machine_days
      |> Enum.map(&get_last_location/1)
  end


  def get_location_for(machine_days, timestamp \\ Timex.now) when is_list(machine_days) do
    machine_days
      |> Enum.map(&(get_location_for(&1, timestamp)))
  end
  def get_location_for(%{} = machine_day, _timestamp), do: machine_day
  def get_location_for(%{locations: nil, current_location: nil} = machine_day, _timestamp), do: machine_day
  def get_location_for(%{locations: locations, current_location: %{}} = machine_day, timestamp) when is_list(locations) do
    machine_day
      |> Map.put(:current_location, nil)
      |> get_location_for(timestamp)
  end
  def get_location_for(%{locations: _locations, current_location: current_location} = machine_day, _timestamp) when is_map(current_location), do: machine_day
  def get_location_for(%{locations: locations, current_location: nil} = machine_day, timestamp) when is_list(locations) do
    current_location = locations
      |> Enum.min_by(fn(loc)->
        diff = Timex.to_unix(timestamp) - Timex.to_unix(loc.timestamp)
        if (diff >= 0) do
          diff
        else
          loc.timestamp
          # Timex.to_unix(loc.timestamp)
        end
      end)
    machine_day
      |> Map.put(:current_location, current_location)
  end


  def lazy_load(%KirovTransport.CDSVyatka.Machine{id: machine_id} = _machine, date \\ Timex.today), do: lazy_load(machine_id, date)
  def lazy_load(%{} = _machine, date), do: lazy_load("", date)
  def lazy_load(machine_id, date) when is_binary(machine_id) do
    machine_day = KirovTransport.CDSVyatka.SingleMachineDay.Query.wo_locations
      |> KirovTransport.CDSVyatka.SingleMachineDay.Query.by_date(date)
      |> KirovTransport.CDSVyatka.SingleMachineDay.Query.for_machine!(machine_id)
    if machine_day do
      struct(KirovTransport.CDSVyatka.SingleMachineDay, machine_day)
    else
      machine_day
    end
  end



  defmodule Query do
    import Ecto.Query

    def wo_locations do
      query = from md in KirovTransport.CDSVyatka.SingleMachineDay,
              # select: [md.date, md.machine_id, md.machine_name, md.enabled, md.last_location]
              select: %{
                id: md.id,
                date: md.date,
                machine_id: md.machine_id,
                machine_name: md.machine_name,
                enabled: md.enabled,
                last_location: md.last_location
              }
      query
    end

    def insert_location(md, new_loc) do
      new_loc = new_loc
        |> apply_changes
        |> Map.from_struct
      # https://stackoverflow.com/questions/42104441/converting-between-ecto-datetime-and-datetime
      timestamp = new_loc[:timestamp]
        |>  Ecto.DateTime.to_erl
        |>  NaiveDateTime.from_erl!
        |> DateTime.from_naive!("Etc/UTC")
      new_loc = new_loc
        |> Map.replace!(:timestamp, timestamp)
      query = from md in KirovTransport.CDSVyatka.SingleMachineDayWithoutLocations,
        where: md.id == ^(md.id),
        update: [push: [locations: ^new_loc]]
      query
    end
    def insert_location!(md, new_loc) do
      # %{query: query, update: update} = test_command(md, new_loc)
      KirovTransport.Repo.update_all(insert_location(md, new_loc), [])
      # KirovTransport.Repo.all(test_command(md, new_loc))
      # KirovTransport.Repo.update!(test_command(md, new_loc))
      # Mongo.Ecto.command(KirovTransport.Repo, [query: query, update: update])
      # Mongo.Ecto.command(KirovTransport.Repo, test_command(md, new_loc))
      # Mongo.find_one_and_update(
      #   KirovTransport.Repo, "machine_days",
      #   %{"id" => md.id}, %{"$addToSet" => %{"locations" => new_loc}}
      # )
    end
    # md = KirovTransport.CDSVyatka.SingleMachineDay.find("5b6bfac61d84ad65f796289c")
    # KirovTransport.CDSVyatka.SingleMachineDay.Query.test_command! md


    def by_date(query, date) do
      query
        |> where([md], md.date == ^date)
    end
    def by_date!(query, date) do
      KirovTransport.Repo.all(by_date(query, date))
    end
    def by_date(date \\ Timex.today) do
      query = from md in KirovTransport.CDSVyatka.SingleMachineDay,
      # query = from md in "machine_days",
            where: md.date == ^date,
            select: md
            # select: %{
            #   id: md.id,
            #   date: md.date,
            #   machine_id: md.machine_id,
            #   machine_name: md.machine_name,
            #   enabled: md.enabled,
            #   last_location: md.last_location
            # }

      # query = query(where: md.id == ^date)
      # KirovTransport.Repo.all(query)
      # KirovTransport.Repo.get_by!(KirovTransport.CDSVyatka.SingleMachineDay, %{date: date})
    end
    def by_date!(date \\ Timex.today) do
      KirovTransport.Repo.all(by_date(date))
    end

    def for_machine(query, machine_id) do
      query
        |> where([md], md.machine_id == ^machine_id)
    end
    def for_machine!(query, machine_id) do
      KirovTransport.Repo.one(for_machine(query, machine_id))
    end
    def for_machine(machine_id) do
      query = from md in KirovTransport.CDSVyatka.SingleMachineDay,
      # query = from md in "machine_days",
            where: md.machine_id == ^machine_id,
            select: md
            # select: [md.date, md.machine_id, md.machine_name, md.enabled, md.last_location]
            # select: take(md, [:date, :machine_id, :machine_name, :enabled, :last_location])
            # select: md.{:date, :machine_id, :machine_name, :enabled, :last_location}
      # KirovTransport.Repo.get_by(KirovTransport.CDSVyatka.SingleMachineDay, %{machine_id: machine_id})
    end
    def for_machine!(machine_id) do
      KirovTransport.Repo.all(for_machine(machine_id))
    end

    # def by_route(route) do
    #   query = from md in KirovTransport.CDSVyatka.SingleMachineDay,
    #   # query = from md in "machine_days",
    #         where: md.marsh == ^route,
    #         select: md
    #   KirovTransport.md.all(query)
    # end

    # # def for_date_and_route(route, date) when is_integer(route) or is_string(route), do: for_date_and_route(date, route)
    # def for_date_and_route(route), do: for_date_and_route(Timex.today, route)
    # def for_date_and_route(route, date) when is_integer(route), do: for_date_and_route(date, route)
    # def for_date_and_route(date, route) do
    #   route = to_string(route)
    #   query = from md in KirovTransport.CDSVyatka.SingleMachineDay,
    #   # query = from md in "machine_days",
    #         where: md.marsh == ^route and md.date == ^date,
    #         select: md
    #   KirovTransport.Repo.all(query)
    #   # KirovTransport.Repo.all(KirovTransport.CDSVyatka.SingleMachineDay, %{marsh: route, date: date})
    # end

    # def for_date_and_machine(machine_id, date \\ Timex.today) do
    #   unless is_binary(machine_id) do
    #     machine_id = machine_id.machine_id
    #   end
    #   for_date_and_machine(date, machine_id)
    # end
    # def for_date_and_machine(%KirovTransport.CDSVyatka.Machine{id: machine_id} = _machine, date \\ Timex.today), do: for_date_and_machine(date, machine_id)
    def for_date_and_machine(date, %KirovTransport.CDSVyatka.Machine{id: machine_id} = _machine), do: for_date_and_machine(date, machine_id)
    def for_date_and_machine(date, machine_id) do
      # IO.inspect date
      # IO.inspect machine_id
      # machine_id = String.to_charlist(machine_id)
      # query = from md in KirovTransport.CDSVyatka.SingleMachineDay,
      # # query = from md in "machine_days",
      #       where: md.machine_id == ^machine_id and md.date == ^date,
      #       # where: [machine_id: ^machine_id, date: ^date],
      #       # where: [date: ^date],
      #       select: md
      # KirovTransport.Repo.one(query)
      KirovTransport.Repo.get_by(KirovTransport.CDSVyatka.SingleMachineDay, %{machine_id: machine_id, date: date})
    end

    def count, do: KirovTransport.Repo.aggregate(KirovTransport.CDSVyatka.SingleMachineDay, :count, :id)
    def all, do: KirovTransport.Repo.all(KirovTransport.CDSVyatka.SingleMachineDay)
  end
end
