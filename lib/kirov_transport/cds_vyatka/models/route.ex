defmodule KirovTransport.CDSVyatka.Route do
  use Ecto.Schema

  alias KirovTransport.CDSVyatka.RoutePoint, as: RoutePoint
  alias __MODULE__.Query, as: Query


  @primary_key {:id, :binary_id, autogenerate: true}
  schema "routes" do
    # field :id
    field :marsh, :string
    field :marshnum, :integer
    field :shortname, :string
    field :smartstop, :boolean, default: true

    embeds_many :points, RoutePoint, on_replace: :delete

    embeds_many :way1_busstops, KirovTransport.CDSVyatka.RouteBusstop, on_replace: :delete
    embeds_many :way2_busstops, KirovTransport.CDSVyatka.RouteBusstop, on_replace: :delete

    field :enabled, :boolean, default: true
    timestamps()
  end

  import Ecto.Changeset
  def changeset(%__MODULE__{} = route , params \\ %{}) do
    route
      |> cast(params, [:marsh, :marshnum, :shortname, :smartstop, :enabled])
      |> cast_embed(:points)
      |> cast_embed(:way1_busstops)
      |> cast_embed(:way2_busstops)
      |> validate_required([:marsh])
  end
  def changeset_new_route(route, params \\ %{}) do
    params = params
      |> Map.put_new("marshnum", rem(String.to_integer(params[:marsh] || params["marsh"]), 1000))

    # params = scrub_params(params)  # change "" to nil
    route
      |> cast(params, [:marsh, :marshnum, :shortname, :smartstop, :enabled])
      |> validate_required([:marsh])
  end


  def add_points(points, route) when is_list(points), do: add_points(route, points)
  def add_points(%__MODULE__{} = route, points) do
    points = points
      |> Enum.map(fn (point) ->
        # if point do
        #   route = add_point(route, point)
        # end
        # point
        RoutePoint.changeset_new_route_point(%RoutePoint{}, point)
      end)

    # HARDFIX for addToSet and << and etcs
    route
      |> change
      |> put_embed(:points, points)
      |> KirovTransport.Repo.update! # changeset
    route
  end


  def add_point(%__MODULE__{} = route_point_params, route) when is_list(route_point_params), do: add_point(route, route_point_params)
  def add_point(%__MODULE__{} = route, route_point_params \\ %{}) do
    # IO.inspect route.points
    route_point = RoutePoint.changeset_new_route_point(%RoutePoint{}, route_point_params)
    # unless has_point_with_num?(route_point.num) do
    unless has_point?(route, route_point.data) do
      # IO.inspect Enum.into([route_point], route.points)
      route = route
        |> change
        |> put_embed(:points, Enum.into([route_point], route.points))
        |> KirovTransport.Repo.update! # changeset
      # Mongo.update_one(KirovTransport.Repo, "routes",
      #   %{"_id": route.id},
      #   %{"$addToSet": %{"points": route_point}}
      # )
    end
    # IO.inspect route.points
    route
  end

  def has_point?(%__MODULE__{} = route, point) do
    has_point_with_num?(route, point.num)
  end
  def has_point_with_num?(%__MODULE__{} = route, point_num) do
    ret = Enum.find(route.points, &(&1.num == point_num))
    ret
  end

  def sorted_points(%__MODULE__{} = route), do: Enum.sort_by(route.points, &(&1.num))
  def reverse_sorted_points(%__MODULE__{} = route), do: Enum.sort_by(route.points, &(&1.num), &>=/2)


  def count, do: Query.count
  def all(%{name: name}), do: Query.by_name(name)
  def all(%{shortname: shortname}), do: Query.by_name(shortname)
  def all(%{marshnum: marshnum}), do: Query.by_marshnum(marshnum)
  def all, do: Query.all
  def delete_all, do: KirovTransport.Repo.delete_all(__MODULE__)
  def find(id), do: KirovTransport.Repo.get!(__MODULE__, id)
  def get_by_route(route), do: Query.by_route(route)

  def add_busstop(%__MODULE__{} = current_route, target, %KirovTransport.CDSVyatka.Busstop{} = busstop, bs) do
    busstops = Map.fetch!(current_route, target)
    route_busstop = busstops
      |> Enum.find(fn(b)->
        b.busstop_id == busstop.id
      end)
    unless route_busstop do
      route_busstop = %KirovTransport.CDSVyatka.RouteBusstop{busstop_id: busstop.id}
        |> KirovTransport.CDSVyatka.RouteBusstop.changeset_new_route_busstop(bs)
      current_route = current_route
        |> Ecto.Changeset.change
        |> Ecto.Changeset.put_embed(target, (busstops ++ [route_busstop]))
    end
    # %{route_busstop: route_busstop, current_route: current_route}
    current_route
  end



  def update_busstops(route) do
    way1_busstops = route.way1_busstops
      |> Enum.map(fn(rbs)->
        KirovTransport.CDSVyatka.RouteBusstop.update_from_busstop(rbs)
      end)
    way2_busstops = route.way2_busstops
      |> Enum.map(fn(rbs)->
        KirovTransport.CDSVyatka.RouteBusstop.update_from_busstop(rbs)
      end)
    # [way1_busstops, way2_busstops]
    route
      |> change
      |> put_embed(:way1_busstops, way1_busstops)
      |> put_embed(:way2_busstops, way2_busstops)
      |> KirovTransport.Repo.update!
  end

  def busstop_kodes(%__MODULE__{way1_busstops: way1_busstops, way2_busstops: way2_busstops} = _route) do
    # stop2<-(stop1-stop1)->stop2 by name (remove clone)
    # if List.last(way1_busstops).stop_name == List.first(way2_busstops).stop_name do
    # stop2<-(stop1-stop1)->stop2 by kod (remove clone)
    way1_last = List.last(way1_busstops)
    way2_first = List.first(way2_busstops)
    if !!way1_last and !!way2_first and way1_last.kod == way2_first.kod do
      [_busstop_clone|way2_busstops] = way2_busstops
    end
    way1_busstops++way2_busstops
      |> Enum.map(&(&1.kod))
  end

  # route = KirovTransport.CDSVyatka.Route.Query.by_route(1001)
  # KirovTransport.CDSVyatka.Route.Query.get_locations(route)
  defmodule Query do
    import Ecto.Query

    alias KirovTransport.CDSVyatka.Route, as: Route
    # def get_locations(route, date \\ Timex.today) when is_binary(route), do: get_locations(%Route{marsh: route}, date)
    def get_locations(%Route{marsh: marsh} = _route, date \\ Timex.today) do
      # marsh = String.to_integer(marsh)
      query = from md in KirovTransport.CDSVyatka.SingleMachineDay,
      # query = from md in "machine_days",
            # where: [date: ^date, last_location: [marsh: ^marsh]],
            # where: md.date == ^date and [last_location: %{marsh: ^marsh}],
            # where: md.date == ^date and fragment("?.? = ?", :last_location, :marsh, marsh),
            where: md.date == ^date and fragment("locations.marsh": ^marsh),
            select: md
      KirovTransport.Repo.all(query)
      # KirovTransport.Repo.get_by(KirovTransport.CDSVyatka.SingleMachineDay, [date: date, "last_location.marsh": marsh])
    end

    def by_route(route) do
      # # query = from r in Route,
      # query = from r in "routes",
      #   where: r.marsh == ^to_string(route),
      #   select: r
      # KirovTransport.Repo.one(query)
      KirovTransport.Repo.get_by(Route, [marsh: to_string(route)])
    end

    def by_marshnum(marshnum) do
      # marshnum = to_string(marshnum)
      # query = from r in "routes",
      query = from r in Route,
        where: r.marshnum == ^marshnum,
        select: r
      KirovTransport.Repo.all(query)
      # KirovTransport.Repo.get_by(Route, %{marshnum: marshnum})
    end

    def by_name(name) do
      name = Mongo.Ecto.Helpers.regex(name, "i")
      # marshnum = to_string(marshnum)
      # query = from r in "routes",
      query = from r in Route,
        where: fragment(shortname: ^name),
        select: r
      KirovTransport.Repo.all(query)
      # KirovTransport.Repo.get_by(Route, %{shortname: name})
    end

    def by_stopname(stopname) do
      name = Mongo.Ecto.Helpers.regex(stopname, "i")
      # marshnum = to_string(marshnum)
      # query = from r in "routes",
      query = from r in Route,
        where: fragment("way1_busstops.stop_name": ^name) or fragment("way2_busstops.stop_name": ^name),
        select: r
      KirovTransport.Repo.all(query)
      # KirovTransport.Repo.get_by(Route, %{shortname: name})
    end

    # params can be lists
    def for_stops(start_busstop, finish_busstop) do
      start_busstop = List.flatten([start_busstop])
      finish_busstop = List.flatten([finish_busstop])
      query = from r in Route,
        where: (
            fragment("way1_busstops.busstop_id": ^%{"$in": start_busstop}) and
            fragment("way1_busstops.busstop_id": ^%{"$in": finish_busstop})
          ) or (
            fragment("way2_busstops.busstop_id": ^%{"$in": start_busstop}) and
            fragment("way2_busstops.busstop_id": ^%{"$in": finish_busstop})
          ),
        # select: r
        select: [r.id, r.shortname]
      KirovTransport.Repo.all(query)
    end
    # start_busstop = "5b5ebd391d84ad38764f394d"
    # finish_busstop = "5b5ebd391d84ad38764f3947"
    # KirovTransport.CDSVyatka.Route.Query.for_stops(start_busstop, finish_busstop)

    # def for_stops2(start_busstop, finish_busstop) do
    #   start_busstop = List.flatten([start_busstop])
    #   finish_busstop = List.flatten([finish_busstop])
    #   query = from r in Route,
    #     where: fragment("function(){this.id === ?}", ^1111),
    #       # (
    #       #   fragment("way1_busstops": ^%{"$elemMatch": %{busstop_id: %{"$in": start_busstop}}}) and
    #       #   fragment("way1_busstops": ^%{"$elemMatch": %{busstop_id: %{"$in": finish_busstop}}})
    #       # ) or (
    #       #   fragment("way2_busstops": ^%{"$elemMatch": %{busstop_id: %{"$in": start_busstop}}}) and
    #       #   fragment("way2_busstops": ^%{"$elemMatch": %{busstop_id: %{"$in": finish_busstop}}})
    #       # ),
    #     # select: r
    #     select: [r.id, r.shortname]
    #   KirovTransport.Repo.all(query)
    # end
    # start_busstop = "5b5ebd391d84ad38764f3924"
    # finish_busstop = "5b5ebd391d84ad38764f3925"
    # KirovTransport.CDSVyatka.Route.Query.for_stops2(start_busstop, finish_busstop)


    # [start_busstop_name1, start_busstop_name2, start_busstop_name3] => /start_busstop_name1|start_busstop_name2|start_busstop_name3/
    def for_stopnames(start_busstop_name, finish_busstop_name) when is_list(start_busstop_name), do: for_stopnames(Enum.join(start_busstop_name, "|"), finish_busstop_name)
    # [finish_busstop_name1, finish_busstop_name2, finish_busstop_name3] => /finish_busstop_name1|finish_busstop_name2|finish_busstop_name3/
    def for_stopnames(start_busstop_name, finish_busstop_name) when is_list(finish_busstop_name), do: for_stopnames(start_busstop_name, Enum.join(finish_busstop_name, "|"))
    # params only strings
    def for_stopnames(start_busstop_name, finish_busstop_name) do
      start_busstop_name = Mongo.Ecto.Helpers.regex(start_busstop_name, "i")
      finish_busstop_name = Mongo.Ecto.Helpers.regex(finish_busstop_name, "i")
      query = from r in Route,
        where: (
            (
              fragment("way1_busstops.stop_name": ^start_busstop_name) or
              fragment("way1_busstops.stop_name_synonyms": ^start_busstop_name)
            ) and (
              fragment("way1_busstops.stop_name": ^finish_busstop_name) or
              fragment("way1_busstops.stop_name_synonyms": ^finish_busstop_name)
            )
          ) or (
            (
              fragment("way2_busstops.stop_name": ^start_busstop_name) or
              fragment("way2_busstops.stop_name_synonyms": ^start_busstop_name)
            ) and (
              fragment("way2_busstops.stop_name": ^finish_busstop_name) or
              fragment("way2_busstops.stop_name_synonyms": ^finish_busstop_name)
            )
          ),
        # select: r
        select: [r.id, r.shortname]
      KirovTransport.Repo.all(query)
    end
    # KirovTransport.CDSVyatka.Route.Query.for_stopnames('name1', 'name2')

    def count, do: KirovTransport.Repo.aggregate(Route, :count, :id)
    def all, do: KirovTransport.Repo.all(Route)
  end
end
