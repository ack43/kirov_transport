defmodule KirovTransport.CDSVyatka.Parser do

  @base_url "https://m.cdsvyatka.com"

  def scheme_url(route), do: "#{@base_url}/scheme.php?marsh=#{route}"
  def route_url(route_path, route), do: "#{@base_url}/#{route_path}?marsh=#{route}"


  # def get_scheme, do: get_scheme(1001)
  def get_scheme(route \\ 1001) do
    # HTTPoison.get!(scheme_url(route), headers())
    case HTTPoison.get(scheme_url(route), headers()) do
    {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
      body = body
        |> Poison.decode
      # {:ok, body}
      body
    {:ok, %HTTPoison.Response{body: body}} ->
      {:error, body}
    {:error, %HTTPoison.Error{reason: reason}} ->
      {:error, reason}
    _ ->
      {:error, "Smth went wrong"}
    end
  end
  # KirovTransport.CDSVyatka.Parser.get_scheme
  def get_scheme!(route \\ 1001) do
    case get_scheme(route) do
      {:ok, scheme} -> scheme
      # {:error, reason} -> raise Error, reason: reason
      {:error, reason} -> raise reason
    end
  end

  def get_busstops_for(marsh) do
    current_route = KirovTransport.CDSVyatka.Route.Query.by_route(marsh)
    busstops = get_scheme!(marsh)["busstop"]
    kods = busstops
      |> Enum.map(&(&1["kod"]))
    if kods != KirovTransport.CDSVyatka.Route.busstop_kodes(current_route) or true do
      current_route
        |> Ecto.Changeset.change
        |> Ecto.Changeset.put_embed(:way1_busstops, [])
        |> Ecto.Changeset.put_embed(:way2_busstops, [])
        |> KirovTransport.Repo.insert_or_update!
      busstops
        |> Enum.map_reduce(%{target: :way1_busstops, prevs: []}, fn(bs, %{target: target, prevs: prevs} = _opts) ->
        # |> Enum.map_reduce(%{target: :way1_busstops, prevs: []}, fn(bs, opts) ->
        #   target = opts.target
        #   prevs = opts.prevs
          busstop = KirovTransport.CDSVyatka.Busstop.Query.by_kod(bs["kod"])
          unless busstop do
            busstop = %KirovTransport.CDSVyatka.Busstop{}
              |> KirovTransport.CDSVyatka.Busstop.changeset_new_busstop(bs)
              |> KirovTransport.Repo.insert!
          end

          current_route = KirovTransport.CDSVyatka.Route.Query.by_route(marsh)
          if target == :way1_busstops do # way1 stops
            prev = Enum.at(prevs, 0)
            old_prev = Enum.at(prevs, 1)
            if !!prev and busstop.stop_name == prev.stop_name do
              # change to way2 stops because stop2<-(stop1-stop1)->stop3
              target = :way2_busstops
            else
              if !!old_prev and busstop.stop_name == old_prev.stop_name do
                # change to way2 stops because (stop2<-stop1->stop2)
                # transform (stop2<-stop1->stop2) to  stop2<-(stop1-stop1)->stop2
                # busstops = current_route.way2_busstops
                # route_busstop = busstops
                #   |> Enum.find(fn(b)->
                #     b.busstop_id == prev.id
                #   end)
                # unless route_busstop do
                #   route_busstop = %KirovTransport.CDSVyatka.RouteBusstop{busstop_id: prev.id}
                #     |> KirovTransport.CDSVyatka.RouteBusstop.changeset_new_route_busstop(Map.from_struct(prev))
                #     # |> KirovTransport.CDSVyatka.RouteBusstop.changeset_new_route_busstop(prev)
                #   current_route = current_route
                #     |> Ecto.Changeset.change
                #     |> Ecto.Changeset.put_embed(:way2_busstops, (busstops ++ [route_busstop]))
                #     |> KirovTransport.Repo.insert_or_update!
                # end
                # %{route_busstop: route_busstop, current_route: current_route} = KirovTransport.CDSVyatka.Route.add_busstop(current_route, :way2_busstops, busstop, bs)
                current_route = KirovTransport.CDSVyatka.Route.add_busstop(current_route, :way2_busstops, busstop, bs)
                  |> KirovTransport.Repo.insert_or_update!
                target = :way2_busstops
              else
                # add_to :way1_busstops
                # busstops = current_route.way1_busstops
                # route_busstop = busstops
                #   |> Enum.find(fn(b)->
                #     b.busstop_id == busstop.id
                #   end)
                # unless route_busstop do
                #   route_busstop = %KirovTransport.CDSVyatka.RouteBusstop{busstop_id: busstop.id}
                #     |> KirovTransport.CDSVyatka.RouteBusstop.changeset_new_route_busstop(bs)
                #   current_route = current_route
                #     |> Ecto.Changeset.change
                #     |> Ecto.Changeset.put_embed(:way1_busstops, (busstops ++ [route_busstop]))
                # end
                # %{route_busstop: route_busstop, current_route: current_route} = KirovTransport.CDSVyatka.Route.add_busstop(current_route, :way1_busstops, busstop, bs)
                current_route = KirovTransport.CDSVyatka.Route.add_busstop(current_route, :way1_busstops, busstop, bs)
              end # if(prevs[1] and busstop.stop_name == prevs[1].stop_name)) do
            end # if(prevs[0] and busstop.stop_name == prevs[0].stop_name)) do
          end
          if target == :way2_busstops do
            # add_to :way2_busstops
            # busstops = current_route.way2_busstops
            # route_busstop = busstops
            #   |> Enum.find(fn(b)->
            #     b.busstop_id == busstop.id
            #   end)
            # unless route_busstop do
            #   route_busstop = %KirovTransport.CDSVyatka.RouteBusstop{busstop_id: busstop.id}
            #     |> KirovTransport.CDSVyatka.RouteBusstop.changeset_new_route_busstop(bs)
            #   current_route = current_route
            #     |> Ecto.Changeset.change
            #     |> Ecto.Changeset.put_embed(:way2_busstops, (busstops ++ [route_busstop]))
            # end
            # %{route_busstop: route_busstop, current_route: current_route} = KirovTransport.CDSVyatka.Route.add_busstop(current_route, :way2_busstops, busstop, bs)
            current_route = KirovTransport.CDSVyatka.Route.add_busstop(current_route, :way2_busstops, busstop, bs)
          end

          if current_route do
            current_route
              |> Ecto.Changeset.change
              |> KirovTransport.Repo.insert_or_update! # changeset
          end
          {:ok, %{target: target, prevs: [busstop] ++ prevs}}
          # [target: target, prevs: [busstop] ++ prevs]
        end)
    end
  end
  def get_busstops do
    get_routes_list!()
      |> Enum.map(fn(r)->
        get_busstops_for(r["marsh"])
      end)
  end
  # KirovTransport.CDSVyatka.Parser.get_busstops

  def get_route_points_for(route) do
    route = KirovTransport.CDSVyatka.Route.Query.by_route(route)
    if route do
      get_scheme!(route.marsh)["scheme"]
        |> KirovTransport.CDSVyatka.Route.add_points(route)
        # |> Enum.map fn (point) ->
        #   IO.inspect route.points
        #   if point do
        #     route = KirovTransport.CDSVyatka.Route.add_point(route, point)
        #   end
        #   point
        # end
    end
  end
  def get_route_points do
    get_routes_list!()
      |> Enum.map fn(r)->
        get_route_points_for(r["marsh"])
      end
  end
  # KirovTransport.CDSVyatka.Parser.get_route_points



  # def get_route(route_path), do: get_route(route_path, 1001)
  def get_route, do: get_route(1001)
  def get_route(route_path) when is_integer(route_path) do
    get_route(mobile_map!()[:route_path], route_path)
  end
  def get_route(route_path) do
    get_route(route_path, 1001)
  end
  def get_route(nil, route), do: get_route(route)
  def get_route(route_path, route) do
    # HTTPoison.get!(route_url(route_path, route), headers())
    case HTTPoison.get(route_url(route_path, route), headers()) do
    {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
      body = body
        |> Poison.decode!
      {:ok, %{body: body, route_path: route_path}}
    {:ok, %HTTPoison.Response{body: body}} ->
      {:error, body}
    {:error, %HTTPoison.Error{reason: reason}} ->
      {:error, reason}
    _ ->
      {:error, "Smth went wrong"}
    end
  end
  # KirovTransport.CDSVyatka.Parser.get_route
  def get_route!, do: get_route!(1001)
  def get_route!(route_path) when is_integer(route_path) do
    get_route!(mobile_map!()[:route_path], route_path)
  end
  def get_route!(route_path) do
    get_route!(route_path, 1001)
  end
  def get_route!(route_path, route) do
    case get_route(route_path, route) do
      {:ok, route} -> route
      # {:error, reason} -> raise Error, reason: reason
      {:error, reason} -> raise reason
    end
  end




  def get_routes_list do
    case HTTPoison.get(@base_url, headers()) do
    {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
      marshdata_code = body
        |> Floki.find("body script")
        |> Enum.at(3)
        |> Floki.text([js: true, deep: false])

        # |> Enum.at(3) |> elem(2) |> Enum.at(0) # tag -> content(as array) -> content (as text)
        # but i found Floki.text [js: true, deep: false]


      init_marshdata_code = "var marshdata=[];marshdata[1]=[];marshdata[2]=[];marshdata[4]=[];"
      output_marshdata_code = """
        var result = [];
        for(var key in marshdata[1]){
          result.push(
            Object.assign(marshdata[1][key], {
              marsh: key
            })
          )
        };
        JSON.stringify(result);
      """

      routes = case init_marshdata_code <>";"<> marshdata_code <>";"<> output_marshdata_code
        |> Execjs.eval
        |> Poison.decode do
          {:ok, routes} ->
            # IO.inspect routes
            routes
          {:error, error} ->
            {:error, error}
          _ ->
            {:error, "Smth went wrong"}
      end
      {:ok, routes}
    {:ok, %HTTPoison.Response{body: body}} ->
      {:error, body}
    {:error, %HTTPoison.Error{reason: reason}} ->
      {:error, reason}
    _ ->
      {:error, "Smth went wrong"}
    end
  end
  # KirovTransport.CDSVyatka.Parser.get_routes_list
  def get_routes_list! do
    case get_routes_list() do
      {:ok, routes_list} -> routes_list
      # {:error, reason} -> raise Error, reason: reason
      {:error, reason} -> raise reason
    end
  end

  def get_routes do
    get_routes_list!()
      |> Enum.map fn (r) ->
        route = KirovTransport.CDSVyatka.Route.Query.by_route(r["marsh"])
        unless route do
          %KirovTransport.CDSVyatka.Route{}
            |> KirovTransport.CDSVyatka.Route.changeset_new_route(r)
            |> KirovTransport.Repo.insert!
        end
      end
  end
  # KirovTransport.CDSVyatka.Parser.get_routes


  # def get_route_info(route \\ 1001) do
  #   calendar_day = KirovTransport.CDSVyatka.CalendarDay.Query.by_date(Timex.today)
  #   unless calendar_day, do: calendar_day = KirovTransport.CDSVyatka.CalendarDay.create(%{date: Timex.today})
  #
  #   machines = get_route!(route)[:body]
  #     |> Enum.map(fn(m)->
  #       m = elem(m, 1)
  #         |>  Map.put("name", elem(m, 0)) # optional
  #
  #       blocks = Regex.run(~r/^(.*?)\<br\>(.*?)\<br\>(.*?)$/, m["info"])
  #         |> tl # without first
  #         |> Enum.map(&(String.trim(&1)))
  #
  #       ## NAME PARSER
  #       name = blocks
  #         |> Enum.at(2)
  #       m = Map.put(m, "name", name)
  #       machine = KirovTransport.CDSVyatka.Machine.get_by_name_or_create(name, m)
  #       machine_day = calendar_day.machines
  #         |> Enum.filter(fn(m)->
  #           # m.machine_id == machine.id
  #           m.machine_name == machine.name
  #         end)
  #         |> Enum.at(0)
  #       unless machine_day do
  #         machine_day_params = %{}
  #           # |> Map.put("machine", machine)
  #           # |> Map.put("machine_id", machine.id)
  #           |> Map.put("machine_name", machine.name)
  #           |> Map.put("marsh", m["marsh"])
  #           |> Map.put("marshnum", m["marshnum"])
  #         machine_day = %KirovTransport.CDSVyatka.MachineDay{machine: machine}
  #         # machine_day = %KirovTransport.CDSVyatka.MachineDay{machine: machine, machine_id: machine.id}
  #         # machine_day = %KirovTransport.CDSVyatka.MachineDay{machine_id: machine.id}
  #           |> KirovTransport.CDSVyatka.MachineDay.changeset_new_machine_day(machine_day_params)
  #           # |> KirovTransport.Repo.insert!
  #         # KirovTransport.CDSVyatka.CalendarDay.add_machine(calendar_day, machine_day)
  #       end
  #       # IO.inspect machine_day
  #
  #       ## TIME PARSER
  #       time = blocks
  #         |> Enum.at(0)
  #         |> String.split(" ")
  #         |> Enum.at(1)
  #       timestamp = Timex.parse!((to_string(Timex.today))<>" "<>(time), "{ISO:Extended}")
  #       # # TODO WTF; we need pattern matching and check changeset
  #       %KirovTransport.CDSVyatka.MachineDay{locations: locations} = machine_day
  #       if locations do
  #         location = locations
  #           |> Enum.filter(fn(l)->
  #             Ecto.DateTime.cast!(timestamp) == Ecto.DateTime.cast!(l.timestamp)
  #           end)
  #           |> Enum.at(0)
  #       else
  #         location = false
  #         locations = []
  #       end
  #       # location = false
  #       # locations = []
  #       unless location do
  #         location_params = m
  #           |> Map.put("timestamp", timestamp)
  #           |> Map.put("lon", m["lng"])
  #         location = %KirovTransport.CDSVyatka.MachineLocation{}
  #           |> KirovTransport.CDSVyatka.MachineLocation.changeset_new_machine_location(location_params)
  #
  #         machine_day = machine_day
  #           |> Ecto.Changeset.change
  #           |> Ecto.Changeset.put_embed(:locations, (locations ++ [location]))
  #       end
  #
  #       machine_day
  #
  #     end)
  #
  #   calendar_day
  #     |> Ecto.Changeset.change
  #     |> Ecto.Changeset.put_embed(:machines, machines)
  #     |> KirovTransport.Repo.update! # changeset
  #
  #   # if length(old_times) > 1 do
  #   #   times
  #   #     |> Enum.zip(old_times)
  #   #     |> Enum.map(fn{a, b}->
  #   #       IO.inspect Timex.to_unix(a)-Timex.to_unix(b)
  #   #       # IO.inspect Timex.Duration.diff(a,b)
  #   #     end)
  #   # end
  # end
  # # recompile; KirovTransport.CDSVyatka.Parser.get_route_info(1001)


  def get_route_info_2(route_path,_) when is_integer(route_path), do: get_route_info_2(nil, route_path)
  def get_route_info_2(route_path \\ nil,route \\ 1001) do
    # today = Timex.to_date(Timex.now) # Timex.today

    (get_route!(route_path, route)[:body] || [])
      |> Enum.map(fn(m)->
        m = elem(m, 1)
          |>  Map.put("name", elem(m, 0)) # optional

        blocks = Regex.run(~r/^(.*?)\<br\>(.*?)\<br\>(.*?)$/, m["info"])
          |> tl # without first (tail)
          |> Enum.map(&(String.trim(&1)))

        ## NAME PARSER
        name = blocks
          |> Enum.at(2)
        m = Map.put(m, "name", name)
        machine = KirovTransport.CDSVyatka.Machine.get_by_name_or_create(name, m)
        # machine_day = KirovTransport.CDSVyatka.SingleMachineDay.Query.for_date_and_machine(Timex.today, machine)
        machine_day = KirovTransport.CDSVyatka.SingleMachineDay.lazy_load(machine, Timex.today)
        unless machine_day do
          machine_day_params = %{}
            |> Map.put("machine_id", machine.id)
            |> Map.put("machine_name", machine.name)
            |> Map.put("date", Timex.today)
          # machine_day = %KirovTransport.CDSVyatka.SingleMachineDay{machine: machine}
          machine_day = %KirovTransport.CDSVyatka.SingleMachineDay{}
            |> KirovTransport.CDSVyatka.SingleMachineDay.changeset_new_machine_day(machine_day_params)
          machine_day = KirovTransport.Repo.insert!(machine_day)
        end
        # IO.inspect machine_day

        ## TIME PARSER
        time = blocks
          |> Enum.at(0)
          |> String.split(" ")
          |> Enum.at(1)
        timestamp = Timex.parse!((to_string(Timex.today))<>" "<>(time), "{ISO:Extended}")

        # machine_day_location = false
        # machine_day_locations = []
        case machine_day do
        %KirovTransport.CDSVyatka.SingleMachineDay{last_location: %{timestamp: last_location_timestamp} = last_location} ->
          if timestamp == last_location_timestamp do
            machine_day_location = last_location
          end
        %{} ->
          machine_day_location = false
          # machine_day_locations = []
        end
        # IO.inspect machine_day_location
        unless machine_day_location do
          location_params = m
            |> Map.put("timestamp", timestamp)
            |> Map.put("lon", m["lng"])
          machine_day_location = %KirovTransport.CDSVyatka.SingleMachineLocation{}
            |> KirovTransport.CDSVyatka.SingleMachineLocation.changeset_new_machine_location(location_params)

          # KirovTransport.CDSVyatka.SingleMachineLocation.insert_location(machine_day, machine_day_location)
          KirovTransport.CDSVyatka.SingleMachineDay.Query.insert_location!(machine_day, machine_day_location)
          # machine_day_locations = (machine_day_locations ++ [machine_day_location])
          machine_day = machine_day
            |> Ecto.Changeset.change
            |> Ecto.Changeset.put_embed(:last_location, machine_day_location)
            # |> Ecto.Changeset.put_embed(:locations, machine_day_locations)
        end

        machine_day
        # machine_day
        #   |> Ecto.Changeset.change
        #   |> KirovTransport.Repo.insert_or_update! # changeset

      end)
  end
  # recompile; KirovTransport.CDSVyatka.Parser.get_route_info_2(1001)

  def get_current_routes(pause) when is_integer(pause), do: get_current_routes(mobile_map!()[:route_path], pause)
  def get_current_routes(current_route_path \\ mobile_map!()[:route_path], pause \\ 100) do
    get_routes_list!()
      |> Enum.map(fn (r) ->
        Process.sleep(pause)
        task = Task.async(fn->
          # route = unless is_integer (r["marsh"]) do
          #   String.to_integer(r["marsh"])
          # else
          #   r["marsh"]
          # end
          route = r["marsh"]
          current_route = KirovTransport.CDSVyatka.Route.Query.by_route(route)
          machine_day = get_route_info_2(current_route_path, current_route.marsh)
          machine_day
          # machine_day
          #   |> Ecto.Changeset.change
          #   |> KirovTransport.Repo.insert_or_update! # changeset
        end)
        task
      end)
      # |> Enum.map(&Task.await/1)
      |> Enum.map(fn(task)->
        Task.await(task)
          |> Enum.map(fn(machine_day)->
            machine_day
              # |> Ecto.Changeset.change
              |> KirovTransport.Repo.update! # changeset
          end)
      end)
  end
  # recompile; KirovTransport.CDSVyatka.Parser.get_current_routes


  def mobile_map(marsh \\ 1001) do
    data = %{
      marsh: marsh,
      bsubmit: "Выбрать",
      scheme: "enable_scheme",
      stopbus: "enable_stopbus",
      city: 1
    } |> Poison.encode!
    headers = %{
      # "Origin": @base_url
    }
    case HTTPoison.post("#{@base_url}/mobile_map.php", data, headers(headers)) do
    {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
      js_with_api_url = body
        |> Floki.find("script")
        |> List.last |> Floki.text([js: true, deep: false])

      js_with_api_url = ~r/\s*options\s*:\s*(\{[^\}]*\})/
        |> Regex.run(js_with_api_url) |> List.last |> String.replace(~r/[\t]+/, " ") |> String.replace(~r/[\n\r]+/, "\n")
      js_with_api_url = "var marsh=#{marsh}; var ajaxObj = #{js_with_api_url}; JSON.stringify(ajaxObj);"

      case js_with_api_url
        |> Execjs.eval
        |> Poison.decode do
          {:ok, ajaxObj} ->
            route_path = ajaxObj["url"]
            |> String.replace(~r/\?.*$/, "")
            {:ok, %{route_path: route_path}}
          {:error, error} ->
            {:error, error}
          _ ->
            {:error, "Smth went wrong"}
      end
    {:ok, %HTTPoison.Response{body: body}} ->
      {:error, body}
    {:error, %HTTPoison.Error{reason: reason}} ->
      {:error, reason}
    _ ->
      {:error, "Smth went wrong"}
    end
  end
  # KirovTransport.CDSVyatka.Parser.mobile_map
  def mobile_map! do
    case mobile_map() do
      {:ok, data} -> data
      # {:error, reason} -> raise Error, reason: reason
      {:error, reason} -> raise reason
    end
  end



  # def headers, do: headers(%{})
  def headers(headers \\ %{}) do
    %{"Referer": @base_url,
      "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
      "Accept-Encoding": "gzip, deflate, br",
      "Accept-Language": "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
      "Cache-Control": "no-cache",
      # "Connection": "keep-alive",
      "Pragma": "no-cache",
      "Upgrade-Insecure-Requests": "1",
      "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.75 Safari/537.36",
      "X-Compress": 1
    } |> Map.merge headers
  end
end
