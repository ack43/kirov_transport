defmodule KirovTransport.Application do
  use Application

  def start(_type, _args) do
    import Supervisor.Spec


    # Define workers and child supervisors to be supervised
    children = [
      supervisor(KirovTransport.Repo, []),
      supervisor(KirovTransportWeb.Endpoint, []),

      # worker(KirovTransport.CDSVyatka.ParserTask, []),

      supervisor(Absinthe.Subscription, [KirovTransportWeb.Endpoint]),

      # 1. Start mongo
      # worker(Mongo, [[database:
      #   Application.get_env(:kirov_transport, :db)[:name], name: :mongo]])
    ]

    opts = [strategy: :one_for_one, name: KirovTransport.Supervisor]
    result = Supervisor.start_link(children, opts)

    # 2. Indexes
    # KirovTransport.Startup.ensure_indexes
    result
  end

  def config_change(changed, _new, removed) do
    KirovTransport.Endpoint.config_change(changed, removed)
    :ok
  end
end
